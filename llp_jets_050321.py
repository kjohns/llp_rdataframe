import ROOT
from ROOT import TFile
import pandas
import datetime
import numpy as np
import time

# note this runs in python3
#tic = time.perf_counter()
tic = time.time()
print (tic)

#ROOT.gROOT.SetStyle('ATLAS') - does not give good results
#from ROOT import *
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.gROOT.LoadMacro("AtlasUtils.C")

# datatype
# 1 or 10 is data
# 2 or 20 is signal mc
# 3 is jet mc
#dofilter = input ('1 to do filters, else 0: ')
#dofilter = int(dofilter)
datatype = 10
print ('datatype = ',datatype)

#infileName = '/home/kjohns/llp_data/signal/aaa.root'
#treeName = 'trees_MS_'
#print (infileName)
#print (treeName)

# enable multi-thread
ROOT.ROOT.EnableImplicitMT()

# add to output files
date = datetime.date.today()
print(type(date))
print (date)
date = str(date)
print(date)

# have to change the input file name as well
if (datatype == 20):
    h_mass = 125.
    s_mass = 55.
    signalname = 'hss'+str(int(h_mass))+'-'+str(int(s_mass))+'-'
    print (signalname)
    outfilename = 'output-'+signalname+date+'.root'
elif (datatype == 3):
    h_mass = 3.
    s_mass = 3.
    outfilename = 'output-jets-'+date+'.root'
elif (datatype == 10):
    h_mass = 10.
    s_mass = 10.
    data_year = '2017'
    outfilename = 'output-data-'+data_year+'-'+date+'.root'
print (outfilename)

if (datatype == 2 or datatype == 20):
    treeName = 'trees_DV_'
else:
    treeName = 'trees_DV_'
 
files = []
files2 = ROOT.std.vector("string")()
#files[0] = "/home/kjohns/llp_data/signal/aaa200.root"
#files[1] = "/home/kjohns/llp_data/signal/bbb200.root"
#files[2] = "/home/kjohns/llp_data/signal/ccc200.root"
#infiles = open('/home/kjohns/llp/rdf-2021/msjetsjets-simple.txt','r')
#infiles = open('/home/kjohns/llp/rdf-2021/msjetsjets-subset-jz5-jz6.txt','r')
#infiles = open('/home/kjohns/llp/rdf-2021/msjetsjets-almost-all.txt','r')
#infiles = open('hss_125_5_all_210104.txt','r')
#infiles = open('hss_125_15_all_210104.txt','r')
#infiles = open('hss_125_35_all_210104.txt','r')
#infiles = open('hss_125_55_high_210104.txt','r')
#infiles = open('hss_125_55_low_210104.txt','r')
#infiles = open('hss_125_55_short_210104.txt','r')
#infiles = open('hss_125_55_all_210104.txt','r')
#infiles = open('hss_60_5_210104.txt','r')
#infiles = open('hss_60_15_210104.txt','r')
#infiles = open('hss_200_50_210104.txt','r')
#infiles = open('hss_400_100_210104.txt','r')
#infiles = open('hss_600_50_210104.txt','r')
#infiles = open('hss_600_150_all_210104.txt','r')
#infiles = open('hss_600_275_210104.txt','r')
#infiles = open('hss_1000_50_210104.txt','r')
#infiles = open('hss_1000_275_all_210104.txt','r')
#infiles = open('hss_1000_475_210104.txt','r')
#infiles = open('/home/kjohns/llp/rdf-2021/data_2015.txt','r')
#infiles = open('/home/kjohns/llp/rdf-2021/data_2016.txt','r')
infiles = open('/home/kjohns/llp/rdf-2021/data_2017.txt','r')
#infiles = open('/home/kjohns/llp/rdf-2021/data_2018.txt','r')
#infiles = open('/home/kjohns/llp/rdf-2021/data_2015_2018.txt','r')
#infiles = open('/home/kjohns/llp/rdf-2021/data_15161718.txt','r')
for line in infiles:
    print (line)
    s = line.strip('\n')
    files.append(s)
    files2.push_back(s)

print (files)
print ('len(files) =',len(files))
print (files2.size())

RDF = ROOT.ROOT.RDataFrame
df = RDF(treeName, files2)
#print (files[0],files[1],files[2])
print (treeName)

#why does this hang my code?
#ROOT.gInterpreter.Declare("""
#//   const int idatatype = int(TPython::Eval("datatype"));
#    int idatatype = int(TPython::Eval("datatype"));
#//    std::cout << idatatype << std::endl;
#""")


# this is an example of a simple c++ function called by python
ROOT.gInterpreter.Declare("""
void hello() {
    std::cout << "hello " << std::endl;
    return; 
};
""")
ROOT.hello()

ROOT.gInterpreter.Declare("""
int create_a_constant(const double python_var) {
    int cpp_var = (int) python_var;
    std::cout << cpp_var << std::endl;
    return cpp_var;
};
""")

ROOT.gInterpreter.Declare("""
int leading_jet_pt_index(const ROOT::RVec<double>& jet_pt) {
    // this is a dummy function to print and do sanity check
    int idx = -1;
    double ptmax = 10.;
    //std::cout << "new event" << std::endl;
    for (std::size_t i=0; i<jet_pt.size(); i++) {
       //std::cout << "i =" << i << "pt = " << jet_pt[i] << std::endl;
       if ( jet_pt[i] > ptmax ) {
         ptmax = jet_pt[i];
         idx = i;
       }
    }
    return idx;
};
""")

ROOT.gInterpreter.Declare("""                                                                                      
double leading_jet_pt(const ROOT::RVec<double>& jet_pt) {
    // this returns element 0 of a vector
    // can be used for other variables besides jet_pt 

    if (jet_pt.size() > 0) {
      return jet_pt[0];
    } else {
      return -1.;
    }
 
    return 0.;
};                                                                                                                                                        
""")

ROOT.gInterpreter.Declare("""
int print_double_vector(const ROOT::RVec<double>& var1) {
    //std::cout << "new event" << std::endl;
    for (std::size_t i=0; i<var1.size(); i++) {
       //std::cout << "i =" << i << "pt = " << var1[i] << std::endl;
    }
     return 0;
};
""")

ROOT.gInterpreter.Declare("""
int msvtx_barrel_hits(const ROOT::RVec<double>& MSVtx_eta, const ROOT::RVec<int>& MSVtx_nMDT, 
    const ROOT::RVec<int>& MSVtx_nRPC, const ROOT::RVec<int>& MSVtx_nTGC) {
    int ipass = 0;
    for (std::size_t i=0; i<MSVtx_eta.size(); i++) {
       if ( (MSVtx_nMDT[i] >= 300 && MSVtx_nMDT[i] < 3000) &&
           MSVtx_nRPC[i] >= 250 &&
           std::abs(MSVtx_eta[i]) < 0.7) 
           ipass = ipass + 1;
     }
    return ipass;
};
""")

ROOT.gInterpreter.Declare("""
int msvtx_endcap_hits(const ROOT::RVec<double>& MSVtx_eta, const ROOT::RVec<int>& MSVtx_nMDT, 
    const ROOT::RVec<int>& MSVtx_nRPC, const ROOT::RVec<int>& MSVtx_nTGC) {
    int ipass = 0;
    for (std::size_t i=0; i<MSVtx_eta.size(); i++) {
       if ( (MSVtx_nMDT[i] >= 300 && MSVtx_nMDT[i] < 3000) &&
           MSVtx_nTGC[i] >= 250 &&
           std::abs(MSVtx_eta[i]) > 1.3 && std::abs(MSVtx_eta[i]) < 2.5)
           ipass = ipass + 1;
     }
    return ipass;
};
""")

ROOT.gInterpreter.Declare("""
int var1_var2_match1(const double drcut, const ROOT::RVec<double>& var1_eta, const ROOT::RVec<double>& var1_phi,\
    const ROOT::RVec<double>& var2_eta, const ROOT::RVec<double>& var2_phi) {
    int ipass = 0;
    double dr = 10.;
//    std::cout << "hello" << std::endl;
//    std::cout << "drcut = " << drcut << std::endl;     
//   var1 must have only one element for this function
//
    if (var1_eta.size() != 1) return ipass;
    for (std::size_t i=0; i<var2_eta.size(); i++) {
         dr = ROOT::VecOps::DeltaR(var1_eta[0], var2_eta[i], var1_phi[0], var2_phi[i]);
//         std::cout << "dr = " << dr << std::endl;
//         std::cout << var1_eta[0] << " " << var2_eta[i] << " " << var1_phi[0] << " " << var2_phi[i] << std::endl;  
         if (dr < drcut) {
            ipass = 1;
         }
    }
    return ipass;
};
""")

ROOT.gInterpreter.Declare("""
int var1_var2_match2(const double drcut, const ROOT::RVec<double>& var1_eta, const ROOT::RVec<double>& var1_phi,\
    const ROOT::RVec<double>& var2_eta, const ROOT::RVec<double>& var2_phi) {
    int imatch[2] = {0,0};
    int idx[2];
    int goodmatch2 = 0;
    double drmin;
    double dr;
    int jmin;

//    std::cout << "hello" << std::endl;
//    std::cout << "drcut = " << drcut << std::endl;     
//   var1 must have 2 elements for this function
//
    if (var1_eta.size() != 2) return goodmatch2;
    for (std::size_t i=0; i<var1_eta.size(); i++) {
         drmin = 10.;
         for (std::size_t j=0; j<var2_eta.size(); j++) {
           dr = ROOT::VecOps::DeltaR(var1_eta[i], var2_eta[j], var1_phi[i], var2_phi[j]);
//         std::cout << "dr = " << dr << std::endl;
//         std::cout << var1_eta[i] << " " << var2_eta[j] << " " << var1_phi[i] << " " << var2_phi[j] << std::endl;  
           if (dr < drmin) {
              jmin = j;
              drmin = dr;
           }
         }
         if (drmin < drcut) {
           imatch[i] = 1;
           idx[i] = jmin;
         }
    }
    if (imatch[0] == 1 && imatch[1] == 1 && idx[0] != idx[1]) {
      goodmatch2 = 1;
    } 
    return goodmatch2;
};
""")

ROOT.gInterpreter.Declare("""
double var1_var2_ptsum(const double drcut, const ROOT::RVec<double>& var1_eta, const ROOT::RVec<double>& var1_phi,\
    const ROOT::RVec<double>& var2_eta, const ROOT::RVec<double>& var2_phi,\
    const ROOT::RVec<double>& var2_pt) {
    double ptsum = 0.;
    double dr = -1;
//   var1 must have only one element for this function
    if (var1_eta.size() != 1) return ptsum;
    for (std::size_t i=0; i<var2_eta.size(); i++) {
         dr = ROOT::VecOps::DeltaR(var1_eta[0], var2_eta[i], var1_phi[0], var2_phi[i]);
         if (dr < drcut) {
            ptsum = ptsum + var2_pt[i];
         }
    }
    return ptsum;
};
""")

# for now, simply use scalar sum of pt
# later change to vector
# scalar sum the pt of tracks or jets opposite the msvtx
# how can i use ROOT math functions in gInterpreter
ROOT.gInterpreter.Declare("""
double var1_var2_ptsum_opposite_side(const double dphicut, const ROOT::RVec<double>& var1_eta, const ROOT::RVec<double>& var1_phi,\
    const ROOT::RVec<double>& var2_eta, const ROOT::RVec<double>& var2_phi,\
    const ROOT::RVec<double>& var2_pt) {
    double ptsum = 999.;
    double dphi = -1.;
    double pi = 3.1415926535897;
//   var1 must have only one element for this function
    if (var1_eta.size() != 1) return ptsum;
    ptsum = 0;
    for (std::size_t i=0; i<var2_eta.size(); i++) {
         dphi = std::abs(var1_phi[0] - var2_phi[i]);
         if (dphi > pi) {
           dphi = dphi - pi;
         }
         if (dphi > dphicut) {
            ptsum = ptsum + var2_pt[i];
         }
    }
    return ptsum;
};
""")
# meant for var2 = met
ROOT.gInterpreter.Declare("""
double var1_var2_dphi(const ROOT::RVec<double>& var1_phi, const double var2_phi) {
    double dphi = -999.;
    double pi = 3.1415926535897;
//   var1 must have only one element for this function
    if (var1_phi.size() != 1) return dphi;
    dphi = 0;
    dphi = std::abs(var1_phi[0] - var2_phi);
         if (dphi > pi) {
           dphi = dphi - pi;
         }
    return dphi;
};
""")

# make more general.  ask if there is a var2 close in dr to var 1.  which
# idx of var2 is returned.  otherwise if nothing close idx -1 is returned.
# both var1 and var2 have to be vectors of doubles though.
#
ROOT.gInterpreter.Declare("""  
ROOT::RVec<int> var1_var2_drdx(const ROOT::RVec<double>& var1_eta, const ROOT::RVec<double>& var1_phi,\
const ROOT::RVec<double>& var2_eta, const ROOT::RVec<double>& var2_phi) {
    /* std::cout << "hello" << std::endl; */
    ROOT::RVec<int> idx;
    ROOT::RVec<double> dr_idx; 
    double dr = 100.;
    int jmin;
    if (var1_eta.size() > 0 && var2_eta.size() > 0) {
        for (std::size_t i = 0; i < var1_eta.size(); i++) {
            auto drmin = 10.;
            for (std::size_t j = 0; j < var2_eta.size(); j++) {
                    dr = ROOT::VecOps::DeltaR(var1_eta[i], var2_eta[j], var1_phi[i], var2_phi[j]);
                    /* std::cout << j << " " << dr << " " << drmin <<std::endl; */ 
                    if (dr < drmin) 
                        {
                          drmin = dr;
                          jmin = j;
                        }
            }
            if (drmin < 0.4)
                {
                    idx.emplace_back(jmin);
                    dr_idx.emplace_back(drmin);
                }
            else
                {
                    idx.emplace_back(-1);    
                    dr_idx.emplace_back(drmin);
                }            
            /* std::cout << idx.size() << " " << idx[0] << " "  << idx[1] << " " << dr_idx[0] << " " << dr_idx[1] << std::endl; */   
        }
    }
    else
    {
        for (std::size_t k = 0; k < var1_eta.size(); k++) { 
           idx.emplace_back(-1);
           dr_idx.emplace_back(10.);
        }
    }
        
    return idx;                                                                                                       
};
""")

ROOT.gInterpreter.Declare("""
ROOT::RVec<double> mult_vec_by_const(const double value, const ROOT::RVec<double>& var1) {
// returns a vector of variable var1 multiplied by a constant value
     
    ROOT::RVec<double> var1_new;
    var1_new = var1;
       for (std::size_t j = 0; j < var1.size(); j++) {
           var1_new[j] = var1[j] * value;
       }
    return var1_new;
};
""")

ROOT.gInterpreter.Declare("""
double mult_scalar_by_const(const double value, const double var1) {
// returns variable var1 multiplied by a constant value

    double var1_new;
    var1_new = var1 * value;
    return var1_new;
};
""")

ROOT.gInterpreter.Declare("""
ROOT::RVec<double> var1_var2_dRclose(const double drcut, const ROOT::RVec<double>& var1_eta, const ROOT::RVec<double>& var1_phi,\
const ROOT::RVec<double>& var2_eta, const ROOT::RVec<double>& var2_phi, const ROOT::RVec<double>& var2_double) {
// return a vector of a variable var2 that is dR close to ONE different variable var1
// assumes 1 entry for var1
// may need to return something if var1 = 0 or var2 = 0
     
    double dr;
    ROOT::RVec<double> var2_dRclose;
//    std::cout << "hello" << std::endl;
    if (var1_eta.size() != 1) {
        var2_dRclose.emplace_back(-999.); 
    } else if (var2_eta.size() == 0) {
        var2_dRclose.emplace_back(-999.);
    } else {
//    if (var1_eta.size() > 0 && var2_eta.size() > 0) {
        for (std::size_t j = 0; j < var2_eta.size(); j++) {
           dr = ROOT::VecOps::DeltaR(var1_eta[0], var2_eta[j], var1_phi[0], var2_phi[j]); 
           if (dr < drcut) {
              var2_dRclose.emplace_back(var2_double[j]);  
           }
        }
    } 
    return var2_dRclose;
};
""") 

ROOT.gInterpreter.Declare("""
ROOT::RVec<double> var1_var2_dRclose_mean(const double drcut, const ROOT::RVec<double>& var1_eta, const ROOT::RVec<double>& var1_phi,\
const ROOT::RVec<double>& var2_eta, const ROOT::RVec<double>& var2_phi) {
// return a vector of size two with mean and rms of the dr values bewteen
// ONE variable var1 and multiple var2
// assumes 1 entry for var1
// may need to return something if var1 = 0 or var2 = 0

    double dr;
    double mean;
    double rms;
    ROOT::RVec<double> dr_close_result_mean;
    ROOT::RVec<double> dr_close_temp;

    if (var1_eta.size() != 1) {
       dr_close_result_mean.emplace_back(-1.);
       dr_close_result_mean.emplace_back(-1.);
//       return dr_close_result_mean;
    } else if (var2_eta.size() == 0) {
       dr_close_result_mean.emplace_back(-1.);
       dr_close_result_mean.emplace_back(-1.);
    } else {
//    std::cout << "hello" << std::endl;
        for (std::size_t j = 0; j < var2_eta.size(); j++) {
           dr = ROOT::VecOps::DeltaR(var1_eta[0], var2_eta[j], var1_phi[0], var2_phi[j]);
           if (dr < drcut) {
              dr_close_temp.emplace_back(dr);
//              std::cout << dr << std::endl;
           }
        }
//  calc mean
//  calc rms
        mean = 0.;
        rms = 0.;
        if (dr_close_temp.size() != 0) {
            for (std::size_t j = 0; j < dr_close_temp.size(); j++) {
                mean = mean + dr_close_temp[j];
                rms = rms + dr_close_temp[j]*dr_close_temp[j];
           }
        mean = mean/((double)dr_close_temp.size());
        rms = std::sqrt(rms/((double)dr_close_temp.size()));
        }
//        std::cout << mean << rms << std::endl;
        dr_close_result_mean.emplace_back(mean);
        dr_close_result_mean.emplace_back(rms);
    }
    return dr_close_result_mean;
};
""")

ROOT.gInterpreter.Declare("""  
double getJetSliceWeight(const int dsid) {
    /* find slice for JZNW samples */

    int slice;
    double weight;
    double multijet_info[39] = {
    // Columns:
    // Evts
    // sigma[nb]
    // epsilon
    47762000, 7.8420E+07, 9.7547E-01,
    47990000, 7.8420E+07, 6.7152E-04,
    47953500, 2.4332E+06, 3.3434E-04,
    47749500, 2.6454E+04, 3.2012E-04,
    39955500, 2.5463E+02, 5.3137E-04,
    39983500, 4.5535E+00, 9.2395E-04,
    43753800, 2.5753E-01, 9.4270E-04,
    35128000, 1.6215E-02, 3.9280E-04,
    35997000, 6.2504E-04, 1.0166E-02,
    33651000, 1.9639E-05, 1.2077E-02,
    31998000, 1.1962E-06, 5.9083E-03,
    31979000, 4.2259E-08, 2.6734E-03,
    31988000, 1.0367E-09, 4.2592E-04
  };

    if (dsid == 361020) {
         slice = 0;
    } else if (dsid == 361021) {
         slice = 1;
    } else if (dsid == 361022) {
         slice = 2;
    } else if (dsid == 361023) {
         slice = 3;
    } else if (dsid == 361024) {
         slice = 4;
    } else if (dsid == 361025) {
         slice = 5;
    } else if (dsid == 361026) {
         slice = 6;
    } else if (dsid == 361027) {
         slice = 7;
    } else if (dsid == 361028) {
         slice = 8;
    } else if (dsid == 361029) {
         slice = 9;
    } else if (dsid == 361030) {
         slice = 10;
    } else if (dsid == 361031) {
         slice = 11;
    } else if (dsid == 361032) {
         slice = 12;
    } else {
         slice = -1;
    }

    double fb = 1.0;
    double b = 1e15 * fb;
    double nb = 1e-9 * b;

    if (slice > 0) {
      double cross_section = multijet_info[slice*3+1] * nb;
      double gen_filt_eff = multijet_info[slice*3+2];
      weight = cross_section / fb * gen_filt_eff;
    } else {
      weight = 1;
    }
    return weight;                                                            
};
""")

ROOT.gInterpreter.Declare("""
ROOT::RVec<double> var1_var2_dRvec(const ROOT::RVec<double>& var1_eta, const ROOT::RVec<double>& var1_phi,\
const ROOT::RVec<double>& var2_eta, const ROOT::RVec<double>& var2_phi, const ROOT::RVec<double>& var2_pt,\
const ROOT::RVec<double>& var2_lograt, const ROOT::RVec<double>& var2_hecf, const ROOT::RVec<int>& var1_nMDT,
const ROOT::RVec<int>& var1_nRPC) {

//  assume there is only ONE of var1.  if not, return vectors of -1.
//  return drmin, 
//  and other variables associated with var2
//  and other (int) variables associated with var1 (called var1_nMDT)
//  put all these in a vector

    ROOT::RVec<double> dr_close_result;
    double drmin = 10.;
    double dr;
    double pt;
    double idx;
    double hecf;
    double nmdt;
    double nrpc;

    if (var1_eta.size() != 1) {
       dr_close_result.emplace_back(-1.);
       dr_close_result.emplace_back(-1.);
       dr_close_result.emplace_back(-1.);
       dr_close_result.emplace_back(-1.);
       dr_close_result.emplace_back(-1.);
       dr_close_result.emplace_back(-1.);
       return dr_close_result;
    } else {
       for (std::size_t j = 0; j < var2_eta.size(); j++) {
          dr = ROOT::VecOps::DeltaR(var1_eta[0], var2_eta[j], var1_phi[0], var2_phi[j]);
          if (dr < drmin) {
             drmin = dr;
             pt = var2_pt[j];
             idx = (double)j;
             hecf = var2_hecf[j];
             //std::cout << idx << " " << pt << std::endl;
          }
       }
      nmdt = (double)var1_nMDT[0];
      nrpc = (double)var1_nRPC[0];
      dr_close_result.emplace_back(drmin);
      dr_close_result.emplace_back(pt);
      dr_close_result.emplace_back(idx);
      dr_close_result.emplace_back(hecf);
      dr_close_result.emplace_back(nmdt);
      dr_close_result.emplace_back(nrpc);
      return dr_close_result;
    }
};
""")

ROOT.gInterpreter.Declare("""
ROOT::RVec<double> var1_var2_dr(const ROOT::RVec<double>& var1_eta, const ROOT::RVec<double>& var1_phi,\
const ROOT::RVec<double>& var2_eta, const ROOT::RVec<double>& var2_phi) {
// i forget why i made this.  other than both var1 and var2 can be > 0

    ROOT::RVec<double> drdx;
    double drmin;
    double dr;
    /* std::cout << "sucka face" << std::endl; */
    if (var1_eta.size() > 0 && var2_eta.size() > 0) {
        for (std::size_t i = 0; i < var1_eta.size(); i++) {
            drmin = 10.;
            for (std::size_t j = 0; j < var2_eta.size(); j++) {
                    dr = ROOT::VecOps::DeltaR(var1_eta[i], var2_eta[j], var1_phi[i], var2_phi[j]);
                    if (dr < drmin)
                        {
                          drmin = dr;
                        }
            }
        drdx.emplace_back(drmin);
        }
    }
    else
      {
        for (std::size_t k = 0; k < var1_eta.size(); k++) {                                
          drdx.emplace_back(10.);     
        }
      }
    return drdx;
};
""")

ROOT.gInterpreter.Declare("""                                                                                        
double var1_var2_dRmin(const ROOT::RVec<double>& var1_eta, const ROOT::RVec<double>& var1_phi,\
const ROOT::RVec<double>& var2_eta, const ROOT::RVec<double>& var2_phi) {

//  assume there is only ONE of var1.  if not, return drmin = -1
//  return drmin
    double drmin = 10.;
    double dr;

    if (var1_eta.size() != 1) {
      drmin = -1.;
    } else {
      for (std::size_t j = 0; j < var2_eta.size(); j++) { 
          dr = ROOT::VecOps::DeltaR(var1_eta[0], var2_eta[j], var1_phi[0], var2_phi[j]);
          if (dr < drmin) {
            drmin = dr;
          }
      }
    }
    return drmin;
};
""")

print ('step 1')
# begin define

myfile = TFile(outfilename,'RECREATE')

if (datatype == 2):
    df_defined_llp = df.Define('nllp','llp_Lx.size()')\
.Define('llp_Lx0','llp_Lx[0]')\
.Define('llp_Lr','sqrt(llp_Lxy*llp_Lxy + llp_Lz*llp_Lz)')\
.Define('llp_DeltaR','ROOT::VecOps::DeltaR(llp_eta[0], llp_eta[1], llp_phi[0], llp_phi[1])')\
.Define('llp_eta_fid','llp_eta[(abs(llp_eta) < 0.7 && llp_Lxy > 3000 && llp_Lxy < 8000) ||\
(abs(llp_eta) > 1.3 && abs(llp_eta) < 2.5 && abs(llp_Lz) > 5000 && abs(llp_Lz) < 15000 \
&& llp_Lxy < 10000)]')\
.Define('llp_phi_fid','llp_phi[(abs(llp_eta) < 0.7 && llp_Lxy > 3000 && llp_Lxy < 8000) ||\
(abs(llp_eta) > 1.3 && abs(llp_eta) < 2.5 && abs(llp_Lz) > 5000 && abs(llp_Lz) < 15000 &\
& llp_Lxy < 10000)]')\
.Define('llp_Lz_fid','llp_Lz[(abs(llp_eta) < 0.7 && llp_Lxy > 3000 && llp_Lxy < 8000) ||\
(abs(llp_eta) > 1.3 && abs(llp_eta) < 2.5 && abs(llp_Lz) > 5000 && abs(llp_Lz) < 15000 &\
& llp_Lxy < 10000)]')\
.Define('llp_Lxy_fid','llp_Lxy[(abs(llp_eta) < 0.7 && llp_Lxy > 3000 && llp_Lxy < 8000) ||\
(abs(llp_eta) > 1.3 && abs(llp_eta) < 2.5 && abs(llp_Lz) > 5000 && abs(llp_Lz)\
 < 15000 && llp_Lxy < 10000)]')\
.Define('llp_Lr_fid','sqrt(llp_Lxy_fid*llp_Lxy_fid + llp_Lz_fid*llp_Lz_fid)')\
.Define('nllp_fid','llp_eta_fid.size()')\
.Define('llp_idx','Combinations(llp_eta,2)')\
.Define('llp_dR','DeltaR(Take(llp_eta,llp_idx[0]),\
Take(llp_eta,llp_idx[1]), Take(llp_phi,llp_idx[0]), Take(llp_phi,llp_idx[1]))')
else:
# for samples without llp truth
    df_defined_llp = df.Define('nllp','0.')

#print (h_mass)
print ('step 2')
if (datatype == 3):
# for jet sample only
# also, add track variables here because they have different names
     df_defined_jet = df_defined_llp.Define('jetsliceweight','getJetSliceWeight(mcChannelNumber)')\
    .Define('njets_weighted','jet_eta.size()*jetsliceweight')\
    .Define('jet_HECF','jet_hecfF')\
    .Define('log10_jetsliceweight','TMath::Log10(jetsliceweight)')\
    .Define('jet_pt_GeV','jet_pt')\
    .Define('leading_jet_pt_idx','leading_jet_pt_index(jet_pt)')\
    .Define('track_pt_GeV','mult_vec_by_const(0.001,track_pt)')\
    .Define('track_g5000_eta','track_eta[track_pt_GeV > 5.]')\
    .Define('track_g5000_phi','track_phi[track_pt_GeV > 5.]')\
    .Define('track_g5000_pt','track_pt_GeV[track_pt_GeV > 5.]')\
    .Define('met_GeV','met_met')\
    .Define('bdt_target','0')\
    .Define('bdt_sample_type','3')
elif (datatype == 20):
# .Define('s_mass','float(TPython::Eval("s_mass"))')
# signal sample
# jetsliceweight = 1.
     df_defined_jet = df_defined_llp.Define('jetsliceweight','1.')\
    .Define('njets_weighted','jet_eta.size()*jetsliceweight')\
    .Define('jet_HECF','jet_hecfF')\
    .Define('log10_jetsliceweight','TMath::Log10(jetsliceweight)')\
    .Define('jet_pt_GeV','jet_pT')\
    .Define('leading_jet_pt_idx','leading_jet_pt_index(jet_pt_GeV)')\
    .Define('track_pt_GeV','track_pT')\
    .Define('track_g5000_eta','track_eta[track_pt_GeV > 5.]')\
    .Define('track_g5000_phi','track_phi[track_pt_GeV > 5.]')\
    .Define('track_g5000_pt','track_pt_GeV[track_pt_GeV > 5.]')\
    .Define('met_GeV','met_met')\
    .Define('bdt_target','1')\
    .Define('bdt_sample_type','20')
elif (datatype == 10):
# data sample
# jetsliceweight = 1.
# note units of pT for jets and tracks    
     df_defined_jet = df_defined_llp.Define('jetsliceweight','1.')\
    .Define('njets_weighted','jet_eta.size()*jetsliceweight')\
    .Define('jet_HECF','jet_hecfF')\
    .Define('log10_jetsliceweight','TMath::Log10(jetsliceweight)')\
    .Define('jet_pt_GeV','jet_pT')\
    .Define('leading_jet_pt_idx','leading_jet_pt_index(jet_pt_GeV)')\
    .Define('track_pt_GeV','track_pT')\
    .Define('track_g5000_eta','track_eta[track_pt_GeV > 5.]')\
    .Define('track_g5000_phi','track_phi[track_pt_GeV > 5.]')\
    .Define('track_g5000_pt','track_pt_GeV[track_pt_GeV > 5.]')\
    .Define('met_GeV','met_met')\
    .Define('bdt_target','0')\
    .Define('bdt_sample_type','10')
print ('step 3')

if (datatype == 3 or datatype == 20 or datatype == 10):
# for jet sample only
    df_defined = df_defined_jet.Define('njets','jet_eta.size()')\
             .Define('nL1MuRoI','L1MuRoI_eta.size()')\
             .Define('nmuRoIClus','muRoIClus_eta.size()')\
             .Define('nmuons','muon_eta.size()')\
             .Define('MSVtx_good_eta','MSVtx_eta[(abs(MSVtx_eta) < 5.)]')\
             .Define('nMSVtx','MSVtx_good_eta.size()')\
             .Define('ntracks','track_eta.size()')\
             .Define('nMSTracklet','MSTracklet_eta.size()')\
             .Define('nMSeg','MSeg_etaPos.size()')\
             .Define('MSVtx_hits_eta','MSVtx_eta[(abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250)\
              || (abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250)]')\
             .Define('MSVtx_hits_phi','MSVtx_phi[(abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250)\
              || (abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250)]')\
             .Define('nMSVtx_hits','MSVtx_hits_eta.size()')\
             .Define('MSVtx_barrel_eta','MSVtx_eta[abs(MSVtx_eta) < 0.7]')\
             .Define('MSVtx_barrel_phi','MSVtx_phi[abs(MSVtx_eta) < 0.7]')\
             .Define('MSVtx_endcap_eta','MSVtx_eta[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5]')\
             .Define('MSVtx_endcap_phi','MSVtx_phi[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5]')\
             .Define('MSVtx_barrel_hits_eta','MSVtx_eta[abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250]')\
             .Define('MSVtx_barrel_hits_phi','MSVtx_phi[abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250]')\
             .Define('MSVtx_barrel_hits_nMDT','MSVtx_nMDT[abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250]')\
             .Define('MSVtx_barrel_hits_nRPC','MSVtx_nRPC[abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250]')\
             .Define('MSVtx_barrel_hits_nMDTnRPC','MSVtx_barrel_hits_nMDT+MSVtx_barrel_hits_nRPC')\
             .Define('MSVtx_barrel_hits_R','MSVtx_R[abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250]')\
              .Define('MSVtx_barrel_hits_z','MSVtx_z[abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250]')\
             .Define('MSVtx_barrel_hits_3dradius','sqrt(MSVtx_barrel_hits_R*MSVtx_barrel_hits_R + MSVtx_barrel_hits_z*MSVtx_barrel_hits_z)')\
             .Define('nMSVtx_barrel_hits','MSVtx_barrel_hits_eta.size()')\
             .Define('MSVtx_endcap_hits_eta','MSVtx_eta[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250]')\
             .Define('MSVtx_endcap_hits_phi','MSVtx_phi[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250]')\
             .Define('MSVtx_endcap_hits_nMDT','MSVtx_nMDT[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250]')\
             .Define('MSVtx_endcap_hits_nTGC','MSVtx_nTGC[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250]')\
             .Define('MSVtx_endcap_hits_nMDTnTGC','MSVtx_endcap_hits_nMDT+MSVtx_endcap_hits_nTGC')\
             .Define('MSVtx_endcap_hits_R','MSVtx_R[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250]')\
             .Define('MSVtx_endcap_hits_z','MSVtx_z[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250]')\
             .Define('MSVtx_endcap_hits_3dradius','sqrt(MSVtx_endcap_hits_R*MSVtx_endcap_hits_R + MSVtx_endcap_hits_z*MSVtx_endcap_hits_z)')\
             .Define('nMSVtx_endcap_hits','MSVtx_endcap_hits_eta.size()')\
             .Define('MSVtx_barrel_hits_vec_jet','var1_var2_dRvec(MSVtx_barrel_hits_eta,MSVtx_barrel_hits_phi,\
             jet_eta,jet_phi,jet_pt_GeV,jet_logRatio,jet_HECF,MSVtx_barrel_hits_nMDT,MSVtx_barrel_hits_nRPC)')\
             .Define('MSVtx_barrel_hits_vec_jet_drmin','MSVtx_barrel_hits_vec_jet[0]')\
             .Define('MSVtx_barrel_hits_vec_jet_pt','MSVtx_barrel_hits_vec_jet[1]')\
             .Define('MSVtx_barrel_hits_vec_jet_idx','MSVtx_barrel_hits_vec_jet[2]')\
             .Define('MSVtx_barrel_hits_vec_jet_hecf','MSVtx_barrel_hits_vec_jet[3]')\
             .Define('MSVtx_barrel_hits_vec_jet_nmdt','MSVtx_barrel_hits_vec_jet[4]')\
             .Define('MSVtx_barrel_hits_vec_jet_nrpc','MSVtx_barrel_hits_vec_jet[5]')\
             .Define('MSVtx_barrel_hits_MSeg_eta','var1_var2_dRclose(1.0,MSVtx_barrel_hits_eta,MSVtx_barrel_hits_phi,\
             MSeg_etaPos,MSeg_phiPos,MSeg_etaPos)')\
             .Define('MSVtx_barrel_hits_nMSeg_eta','MSVtx_barrel_hits_MSeg_eta.size()')\
             .Define('MSVtx_barrel_hits_MSTracklet_eta','var1_var2_dRclose(1.0,MSVtx_barrel_hits_eta,MSVtx_barrel_hits_phi,\
             MSTracklet_eta,MSTracklet_phi,MSTracklet_eta)')\
             .Define('MSVtx_barrel_hits_nMSTracklet_eta','MSVtx_barrel_hits_MSTracklet_eta.size()')\
             .Define('MSVtx_barrel_hits_g5track_drmin','var1_var2_dRmin(MSVtx_barrel_hits_eta,MSVtx_barrel_hits_phi,\
             track_g5000_eta,track_g5000_phi)')\
             .Define('MSVtx_barrel_hits_g5track_ptsum_opposite','var1_var2_ptsum_opposite_side(2.356,MSVtx_barrel_hits_eta,MSVtx_barrel_hits_phi,\
             track_g5000_eta,track_g5000_phi,track_g5000_pt)')\
             .Define('MSVtx_barrel_hits_dr_vec_MSeg','var1_var2_dRclose_mean(1.0,MSVtx_barrel_hits_eta,MSVtx_barrel_hits_phi,\
             MSeg_etaPos,MSeg_phiPos)')\
             .Define('MSVtx_barrel_hits_dr_mean_MSeg','MSVtx_barrel_hits_dr_vec_MSeg[0]')\
             .Define('MSVtx_barrel_hits_dr_rms_MSeg','MSVtx_barrel_hits_dr_vec_MSeg[1]')\
             .Define('MSVtx_barrel_hits_dr_vec_MSTracklet','var1_var2_dRclose_mean(1.0,MSVtx_barrel_hits_eta,MSVtx_barrel_hits_phi,\
             MSTracklet_eta,MSTracklet_phi)')\
             .Define('MSVtx_barrel_hits_dr_mean_MSTracklet','MSVtx_barrel_hits_dr_vec_MSTracklet[0]')\
             .Define('MSVtx_barrel_hits_dr_rms_MSTracklet','MSVtx_barrel_hits_dr_vec_MSTracklet[1]')\
             .Define('MSVtx_barrel_hits_met_dphi','var1_var2_dphi(MSVtx_barrel_hits_phi,met_phi)')\
             .Define('MSVtx_endcap_hits_vec_jet','var1_var2_dRvec(MSVtx_endcap_hits_eta,MSVtx_endcap_hits_phi,\
             jet_eta,jet_phi,jet_pt_GeV,jet_logRatio,jet_HECF,MSVtx_endcap_hits_nMDT,MSVtx_endcap_hits_nTGC)')\
             .Define('MSVtx_endcap_hits_vec_jet_drmin','MSVtx_endcap_hits_vec_jet[0]')\
             .Define('MSVtx_endcap_hits_vec_jet_pt','MSVtx_endcap_hits_vec_jet[1]')\
             .Define('MSVtx_endcap_hits_vec_jet_idx','MSVtx_endcap_hits_vec_jet[2]')\
             .Define('MSVtx_endcap_hits_vec_jet_hecf','MSVtx_endcap_hits_vec_jet[3]')\
             .Define('MSVtx_endcap_hits_vec_jet_nmdt','MSVtx_endcap_hits_vec_jet[4]')\
             .Define('MSVtx_endcap_hits_vec_jet_ntgc','MSVtx_endcap_hits_vec_jet[5]')\
             .Define('MSVtx_endcap_hits_MSeg_eta','var1_var2_dRclose(1.0,MSVtx_endcap_hits_eta,MSVtx_endcap_hits_phi,\
             MSeg_etaPos,MSeg_phiPos,MSeg_etaPos)')\
             .Define('MSVtx_endcap_hits_nMSeg_eta','MSVtx_endcap_hits_MSeg_eta.size()')\
             .Define('MSVtx_endcap_hits_MSTracklet_eta','var1_var2_dRclose(1.0,MSVtx_endcap_hits_eta,MSVtx_endcap_hits_phi,\
             MSTracklet_eta,MSTracklet_phi,MSTracklet_eta)')\
             .Define('MSVtx_endcap_hits_nMSTracklet_eta','MSVtx_endcap_hits_MSTracklet_eta.size()')\
             .Define('MSVtx_endcap_hits_g5track_drmin','var1_var2_dRmin(MSVtx_endcap_hits_eta,MSVtx_endcap_hits_phi,\
             track_g5000_eta,track_g5000_phi)')\
             .Define('MSVtx_endcap_hits_g5track_ptsum_opposite','var1_var2_ptsum_opposite_side(2.356,MSVtx_endcap_hits_eta,MSVtx_endcap_hits_phi,\
             track_g5000_eta,track_g5000_phi,track_g5000_pt)')\
             .Define('MSVtx_endcap_hits_dr_vec_MSeg','var1_var2_dRclose_mean(1.0,MSVtx_endcap_hits_eta,MSVtx_endcap_hits_phi,\
             MSeg_etaPos,MSeg_phiPos)')\
             .Define('MSVtx_endcap_hits_dr_mean_MSeg','MSVtx_endcap_hits_dr_vec_MSeg[0]')\
             .Define('MSVtx_endcap_hits_dr_rms_MSeg','MSVtx_endcap_hits_dr_vec_MSeg[1]')\
             .Define('MSVtx_endcap_hits_dr_vec_MSTracklet','var1_var2_dRclose_mean(1.0,MSVtx_endcap_hits_eta,MSVtx_endcap_hits_phi,\
             MSTracklet_eta,MSTracklet_phi)')\
             .Define('MSVtx_endcap_hits_dr_mean_MSTracklet','MSVtx_endcap_hits_dr_vec_MSTracklet[0]')\
             .Define('MSVtx_endcap_hits_dr_rms_MSTracklet','MSVtx_endcap_hits_dr_vec_MSTracklet[1]')\
             .Define('MSVtx_endcap_hits_met_dphi','var1_var2_dphi(MSVtx_endcap_hits_phi,met_phi)')
else:
    df_defined = df_defined_jet.Define('njets','jet_eta.size()')\
             .Define('nL1MuRoI','L1MuRoI_eta.size()')\
             .Define('nmuRoIClus','muRoIClus_eta.size()')\
             .Define('nmuons','muon_eta.size()')\
             .Define('nMSVtx','MSVtx_eta.size()')\
             .Define('ntracks','track_eta.size()')\
             .Define('nMSTracklet','MSTracklet_eta.size()')\
             .Define('nMSeg','MSeg_etaPos.size()')\
             .Define('track_pt400','track_pt[track_pt > 400]')\
             .Define('ntracks_pt400','track_pt400.size()')\
             .Define('goodPV','hasGoodPV')\
             .Define('isClean','isCompleteEvent && isGoodLAr && isGoodSCT && isGoodTile')\
             .Define('muRoIClus_barrel_eta','muRoIClus_eta[abs(muRoIClus_eta) < 0.7]')\
             .Define('muRoIClus_barrel_phi','muRoIClus_phi[abs(muRoIClus_eta) < 0.7]')\
             .Define('muRoIClus_endcap_eta','muRoIClus_eta[abs(muRoIClus_eta) > 1.3 && abs(muRoIClus_eta) < 2.5]')\
             .Define('muRoIClus_endcap_phi','muRoIClus_phi[abs(muRoIClus_eta) > 1.3 && abs(muRoIClus_eta) < 2.5]')\
             .Define('MSVtx_barrel_eta','MSVtx_eta[abs(MSVtx_eta) < 0.7]')\
             .Define('MSVtx_barrel_phi','MSVtx_phi[abs(MSVtx_eta) < 0.7]')\
             .Define('MSVtx_endcap_eta','MSVtx_eta[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5]')\
             .Define('MSVtx_endcap_phi','MSVtx_phi[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5]')\
             .Define('MSVtx_barrel_hits_eta','MSVtx_eta[abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250]')\
             .Define('MSVtx_barrel_hits_phi','MSVtx_phi[abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250]')\
             .Define('MSVtx_barrel_hits_nMDT','MSVtx_nMDT[abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250]')\
             .Define('MSVtx_barrel_hits_nRPC','MSVtx_nRPC[abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250]')\
             .Define('MSVtx_barrel_hits_nMDTnRPC','MSVtx_barrel_hits_nMDT+MSVtx_barrel_hits_nRPC')\
             .Define('MSVtx_barrel_hits_R','MSVtx_R[abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250]')\
              .Define('MSVtx_barrel_hits_z','MSVtx_z[abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250]')\
             .Define('MSVtx_barrel_hits_3dradius','sqrt(MSVtx_barrel_hits_R*MSVtx_barrel_hits_R + MSVtx_barrel_hits_z*MSVtx_barrel_hits_z)')\
             .Define('nMSVtx_barrel_hits','MSVtx_barrel_hits_eta.size()')\
             .Define('MSVtx_endcap_hits_eta','MSVtx_eta[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250]')\
             .Define('MSVtx_endcap_hits_phi','MSVtx_phi[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250]')\
             .Define('MSVtx_endcap_hits_nMDT','MSVtx_nMDT[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250]')\
             .Define('MSVtx_endcap_hits_nTGC','MSVtx_nTGC[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250]')\
             .Define('MSVtx_endcap_hits_nMDTnTGC','MSVtx_endcap_hits_nMDT+MSVtx_endcap_hits_nTGC')\
             .Define('MSVtx_endcap_hits_R','MSVtx_R[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250]')\
             .Define('MSVtx_endcap_hits_z','MSVtx_z[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250]')\
             .Define('MSVtx_endcap_hits_3dradius','sqrt(MSVtx_endcap_hits_R*MSVtx_endcap_hits_R + MSVtx_endcap_hits_z*MSVtx_endcap_hits_z)')\
             .Define('nMSVtx_endcap_hits','MSVtx_endcap_hits_eta.size()')\
             .Define('MSVtx_iso_barrel_hits_eta','MSVtx_eta[abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250 && MSVtx_passAllIso]')\
             .Define('MSVtx_iso_barrel_hits_phi','MSVtx_phi[abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250 && MSVtx_passAllIso]')\
             .Define('MSVtx_iso_endcap_hits_eta','MSVtx_eta[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250 && MSVtx_passAllIso]')\
             .Define('MSVtx_iso_endcap_hits_phi','MSVtx_phi[abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250 && MSVtx_passAllIso]')\
             .Define('MSVtx_iso_hits_eta','MSVtx_eta[(abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250 && MSVtx_passAllIso)\
              || (abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250 && MSVtx_passAllIso)]')\
             .Define('MSVtx_iso_hits_phi','MSVtx_phi[(abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250 && MSVtx_passAllIso)\
              || (abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250 && MSVtx_passAllIso)]')\
             .Define('nMSVtx_iso_hits','MSVtx_iso_hits_eta.size()')\
             .Define('MSVtx_hits_eta','MSVtx_eta[(abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250)\
              || (abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250)]')\
             .Define('MSVtx_hits_phi','MSVtx_phi[(abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250)\
              || (abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250)]')\
             .Define('MSVtx_hits_nMDT','MSVtx_nMDT[(abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250)\
              || (abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250)]')\
             .Define('MSVtx_hits_R','MSVtx_R[(abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250)\
              || (abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250)]')\
             .Define('MSVtx_hits_z','MSVtx_z[(abs(MSVtx_eta) < 0.7\
              && MSVtx_R > 3000 && MSVtx_R < 8000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nRPC > 250)\
              || (abs(MSVtx_eta) > 1.3 && abs(MSVtx_eta) < 2.5\
              && MSVtx_R < 10000 && abs(MSVtx_z) > 5000 && abs(MSVtx_z) < 15000\
              && MSVtx_nMDT > 300 && MSVtx_nMDT < 3000 && MSVtx_nTGC > 250)]')\
             .Define('MSVtx_hits_3dradius','sqrt(MSVtx_hits_R*MSVtx_hits_R + MSVtx_hits_z*MSVtx_hits_z)')\
             .Define('nMSVtx_hits','MSVtx_hits_eta.size()')\
             .Define('loose_cluster_jets_eta','jet_eta[jet_pt_GeV > 17. && jet_logRatio < 0.5]')\
             .Define('loose_cluster_jets_phi','jet_phi[jet_pt_GeV > 17. && jet_logRatio < 0.5]')\
             .Define('tight_cluster_jets_eta','jet_eta[jet_pt_GeV > 30. && jet_logRatio < 0.5 && jet_passJvt]')\
             .Define('tight_cluster_jets_phi','jet_phi[jet_pt_GeV > 30. && jet_logRatio < 0.5 && jet_passJvt]')\
             .Define('njets_loose_cluster','loose_cluster_jets_eta.size()')\
             .Define('track_g5000_eta','track_eta[track_pt > 5000.]')\
             .Define('track_g5000_phi','track_phi[track_pt > 5000.]')\
             .Define('track_l5000_eta','track_eta[track_pt < 5000.]')\
             .Define('track_l5000_phi','track_phi[track_pt < 5000.]')\
             .Define('logratio_jet_pt','jet_pt[jet_logRatio > 0.5]')\
             .Define('logratio_njets','logratio_jet_pt.size()')\
             .Define('leading_jet_pt','get_leading_jet_pt(jet_pt_GeV)')\
             .Define('leading_logratio_jet_pt','get_leading_jet_pt(logratio_jet_pt)')\
             .Define('logratio_jet_eta','jet_eta[jet_logRatio > 0.5]')\
             .Define('logratio_jet_phi','jet_phi[jet_logRatio > 0.5]')\
             .Define('MSTracklet_pT_jet_match','var1_var2_dRclose(0.6,jet_eta,jet_phi,MSTracklet_eta,MSTracklet_phi,MSTracklet_pT)')\
             .Define('MSTracklet_pT_logratio_jet_match','var1_var2_dRclose(0.6,logratio_jet_eta,logratio_jet_phi,\
             MSTracklet_eta,MSTracklet_phi,MSTracklet_pT)')\
             .Define('nMSTracklet_pT_jet_match','MSTracklet_pT_jet_match.size()')\
             .Define('nMSTracklet_pT_logratio_jet_match','MSTracklet_pT_logratio_jet_match.size()')\
             .Define('MSeg_etaPos_jet_match','var1_var2_dRclose(0.6,jet_eta,jet_phi,MSeg_etaPos,MSeg_phiPos,MSeg_etaPos)')\
             .Define('MSeg_etaPos_logratio_jet_match','var1_var2_dRclose(0.6,jet_eta,jet_phi,MSeg_etaPos,MSeg_phiPos,MSeg_etaPos)')\
             .Define('nMSeg_etaPos_jet_match','MSeg_etaPos_jet_match.size()')\
             .Define('nMSeg_etaPos_logratio_jet_match','MSeg_etaPos_logratio_jet_match.size()')\
             .Define('MSVtx_barrel_hits_vec_jet','var1_var2_dRvec(MSVtx_barrel_hits_eta,MSVtx_barrel_hits_phi,\
             jet_eta,jet_phi,jet_pt_GeV,jet_logRatio,jet_HECF,MSVtx_barrel_hits_nMDTnRPC)')\
             .Define('MSVtx_barrel_hits_vec_jet_drmin','MSVtx_barrel_hits_vec_jet[0]')\
             .Define('MSVtx_barrel_hits_vec_jet_pt','MSVtx_barrel_hits_vec_jet[1]')\
             .Define('MSVtx_barrel_hits_vec_jet_lograt','MSVtx_barrel_hits_vec_jet[2]')\
             .Define('MSVtx_barrel_hits_vec_jet_hecf','MSVtx_barrel_hits_vec_jet[3]')\
             .Define('MSVtx_barrel_hits_vec_jet_nmdt','MSVtx_barrel_hits_vec_jet[4]')\
             .Define('MSVtx_barrel_hits_MSeg_eta','var1_var2_dRclose(0.4,MSVtx_barrel_hits_eta,MSVtx_barrel_hits_phi,\
             MSeg_etaPos,MSeg_phiPos,MSeg_etaPos)')\
             .Define('nMSVtx_barrel_hits_MSeg_eta','MSVtx_barrel_hits_MSeg_eta.size()')\
             .Define('MSVtx_barrel_hits_g5track_drmin','var1_var2_dRmin(MSVtx_barrel_hits_eta,MSVtx_barrel_hits_phi,\
             track_g5000_eta,track_g5000_phi)')\
             .Define('MSVtx_endcap_hits_vec_jet','var1_var2_dRvec(MSVtx_endcap_hits_eta,MSVtx_endcap_hits_phi,\
             jet_eta,jet_phi,jet_pt_GeV,jet_logRatio,jet_HECF,MSVtx_endcap_hits_nMDTnTGC)')\
             .Define('MSVtx_endcap_hits_vec_jet_drmin','MSVtx_endcap_hits_vec_jet[0]')\
             .Define('MSVtx_endcap_hits_vec_jet_pt','MSVtx_endcap_hits_vec_jet[1]')\
             .Define('MSVtx_endcap_hits_vec_jet_lograt','MSVtx_endcap_hits_vec_jet[2]')\
             .Define('MSVtx_endcap_hits_vec_jet_hecf','MSVtx_endcap_hits_vec_jet[3]')\
             .Define('MSVtx_endcap_hits_vec_jet_nmdt','MSVtx_endcap_hits_vec_jet[4]')\
             .Define('MSVtx_endcap_hits_MSeg_eta','var1_var2_dRclose(0.4,MSVtx_endcap_hits_eta,MSVtx_endcap_hits_phi,\
             MSeg_etaPos,MSeg_phiPos,MSeg_etaPos)')\
             .Define('nMSVtx_endcap_hits_MSeg_eta','MSVtx_endcap_hits_MSeg_eta.size()')\
             .Define('MSVtx_endcap_hits_g5track_drmin','var1_var2_dRmin(MSVtx_endcap_hits_eta,MSVtx_endcap_hits_phi,\
             track_g5000_eta,track_g5000_phi)')

print ('step 4')

if (datatype == 2):
    df_defined_llp2 = df_defined.Define('MSVtx_hits_vec_llp','var1_var2_dRvec(MSVtx_hits_eta,MSVtx_hits_phi,\
llp_eta,llp_phi,llp_Lxy,llp_Lz,llp_Lz,MSVtx_hits_nMDT)')\
             .Define('MSVtx_hits_vec_llp_drmin','MSVtx_hits_vec_llp[0]')\
             .Define('MSVtx_hits_vec_llp_Lxy','MSVtx_hits_vec_llp[1]')\
             .Define('MSVtx_hits_vec_llp_Lz','MSVtx_hits_vec_llp[2]')\
             .Define('MSVtx_hits_vec_llp_nmdt','MSVtx_hits_vec_llp[3]')\
             .Define('LLP_MSVtx_dxy','MSVtx_hits_vec_llp_Lxy - MSVtx_hits_R[0]')\
             .Define('LLP_MSVtx_dz','MSVtx_hits_vec_llp_Lz - MSVtx_hits_z[0]')

#             .Define('idx_llp_l1muroi','var1_var2_drdx(llp_eta,llp_phi,L1MuRoI_eta,L1MuRoI_phi)')\
#             .Define('idx_llp0_l1muroi','idx_llp_l1muroi[0]')\
#             .Define('idx_llp_barrel_muroiclus','var1_var2_drdx(llp_eta_barrel_fid,llp_phi_barrel_fid,muRoIClus_eta,muRoIClus_phi)')\
#             .Define('idx_llp_endcap_muroiclus','var1_var2_drdx(llp_eta_endcap_fid,llp_phi_endcap_fid,muRoIClus_eta,muRoIClus_phi)')\
#             .Define('idx_llp_msvtx','var1_var2_drdx(llp_eta,llp_phi,MSVtx_eta,MSVtx_phi)')\
#             .Define('llp_muroi_dr','var1_var2_dr(llp_eta,llp_phi,L1MuRoI_eta,L1MuRoI_phi)')\
#             .Define('llp0_muroi_dr','llp_muroi_dr[0]')\
#             .Define('llp_lxy_barrel_l1muroi','llp_Lxy[idx_llp_l1muroi >= 0 && abs(llp_eta) < 0.7 && llp_Lxy > 3000 && llp_Lxy < 8000]')\
#             .Define('llp_lxy_endcap_l1muroi','llp_Lxy[idx_llp_l1muroi >= 0 && abs(llp_eta) > 1.3 &&\
#               abs(llp_eta) < 2.5 && abs(llp_Lz) > 5000 && abs(llp_Lz) < 15000 && llp_Lxy < 10000]')\
#             .Define('llp_lxy_barrel_muroiclus','llp_lxy_barrel_fid[idx_llp_barrel_muroiclus >= 0]')\
#             .Define('llp_lxy_endcap_muroiclus','llp_lxy_endcap_fid[idx_llp_endcap_muroiclus >= 0]')\
#             .Define('llp_lxy_barrel_msvtx','llp_Lxy[idx_llp_msvtx >= 0 && abs(llp_eta) < 0.7 && llp_Lxy > 3000 && llp_Lxy < 8000]')\
#             .Define('llp_lxy_endcap_msvtx','llp_Lxy[idx_llp_msvtx >= 0 && abs(llp_eta) > 1.3 &&\
#                abs(llp_eta) < 2.5 && abs(llp_Lz) > 5000 && abs(llp_Lz) < 15000 && llp_Lxy < 10000]')
#print (df_defined2.AsNumpy(['jdx']))

# cuts
# run number cuts
#runnum_cut1 = 'runNumber == 303264 || runNumber == 302831'
#runnum_cut1 = 'runNumber == 331772 || runNumber == 363664'
runnum_cut1 = 'runNumber != -1'
entries = df.Filter(runnum_cut1).Count()
print("%s total entries" %entries.GetValue())
# cross check
entries = df_defined.Filter(runnum_cut1).Count()
print("%s total entries" %entries.GetValue())

# msvtx only one
msvtx_cut1 = 'nMSVtx_hits == 1'
msvtx_barrel_hits_cut1 = 'nMSVtx_barrel_hits == 1'
msvtx_endcap_hits_cut1 = 'nMSVtx_endcap_hits == 1'
entries = df_defined.Filter(msvtx_barrel_hits_cut1).Count()
print("%s msvtx_harrel_hits_cut1 entries" %entries.GetValue())
entries = df_defined.Filter(msvtx_endcap_hits_cut1).Count()
print("%s msvtx_endcap_hits_cut1 entries" %entries.GetValue())

# met cuts
met_cut1 = 'met_GeV < 40'
# if > 0 one will lose some events
# met_cut1 = 'met_GeV > -1'
entries = df_defined.Filter(met_cut1).Count()
print("%s met_cut1 entries" %entries.GetValue())
entries = df_defined.Filter(msvtx_barrel_hits_cut1).Filter(met_cut1).Count()
print("%s msvtx_harrel_hits_cut1 AND met cut1 entries" %entries.GetValue())
entries = df_defined.Filter(msvtx_endcap_hits_cut1).Filter(met_cut1).Count()
print("%s msvtx_endcap_hits_cut1 AND met cut1 entries" %entries.GetValue())

# drmin cuts
msvtx_barrel_hits_drmin_cuts1 = 'nMSVtx_barrel_hits == 1 && MSVtx_barrel_hits_vec_jet_drmin > 0.5 && \
MSVtx_barrel_hits_g5track_drmin > 0.5'
entries = df_defined.Filter(msvtx_barrel_hits_drmin_cuts1).Count()
print("%s msvtx_barrel_hits_drmin_cuts1 entries" %entries.GetValue()) 
msvtx_endcap_hits_drmin_cuts1 = 'nMSVtx_endcap_hits == 1 && MSVtx_endcap_hits_vec_jet_drmin > 0.5 && \
MSVtx_endcap_hits_g5track_drmin > 0.5'
entries = df_defined.Filter(msvtx_endcap_hits_drmin_cuts1).Count()
print("%s msvtx_endcap_hits_drmin_cuts1 entries" %entries.GetValue())

# roi trigger
muon_roi_cluster_trigger_cut1 = 'pass_HLT_j30_muvtx_noiso == 1'
entries = df_defined.Filter(muon_roi_cluster_trigger_cut1).Count()
print("%s cluster trigger cut entries" %entries.GetValue())

# pv cut
pv_cut1 = 'hasGoodPV == 1'
entries = df_defined.Filter(pv_cut1).Count()
print("%s one good PV entries" %entries.GetValue())

# cleaning flags
clean_cut1 = 'hasGoodPV && isGoodLAr && isGoodSCT && isGoodTile && isCompleteEvent'
entries = df_defined.Filter(clean_cut1).Count()
print("%s cleaning flags cut (except JetClean) entries" %entries.GetValue())

# msvtx only one AND roi trigger
entries = df_defined.Filter(muon_roi_cluster_trigger_cut1).Filter(msvtx_barrel_hits_cut1).Count()
print("%s msvtx_harrel_hits_cut1 AND muon_roi_cluster_trigger_cut1 entries" %entries.GetValue())
entries = df_defined.Filter(muon_roi_cluster_trigger_cut1).Filter(msvtx_endcap_hits_cut1).Count()
print("%s msvtx_endcap_hits_cut1 AND muon_roi_cluster_trigger_cut1 entries" %entries.GetValue())

# msvtx only one AND met cut AND cleaning cuts AND drmin_cuts
entries = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_drmin_cuts1).Filter(msvtx_barrel_hits_cut1).Count()
print("%s msvtx_harrel_hits_cut1 AND clean_cut1 AND met_cut1 AND drmin_cuts1 entries" %entries.GetValue())
entries = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_drmin_cuts1).Filter(msvtx_endcap_hits_cut1).Count()
print("%s msvtx_endcap_hits_cut1 AND clean_cut1 AND met_cut1 AND drmin_cuts1 entries" %entries.GetValue())

# selection flag
selection_barrel_cut1 = 'hasGoodPV && isGoodLAr && isGoodSCT && isGoodTile && isCompleteEvent\
 && met_GeV < 40  && MSVtx_barrel_hits_vec_jet_drmin > 0.5 && MSVtx_barrel_hits_g5track_drmin > 0.5\
 && nMSVtx_barrel_hits == 1'
selection_endcap_cut1 = 'hasGoodPV && isGoodLAr && isGoodSCT && isGoodTile && isCompleteEvent\
 && met_GeV < 40 && MSVtx_endcap_hits_vec_jet_drmin > 0.5 && MSVtx_endcap_hits_g5track_drmin > 0.5\
 && nMSVtx_endcap_hits == 1' 
entries = df_defined.Filter(selection_barrel_cut1).Count()
print("%s selection barrel cut1 entries" %entries.GetValue())
entries = df_defined.Filter(selection_endcap_cut1).Count()
print("%s selection endcap cut1 entries" %entries.GetValue())

# exit()

# begin histograms

if (datatype == 1):
    h_nMSeg = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Histo1D(('nMSeg','nMSeg',50,0.,500.),'nMSeg')
    h_MSeg_etaPos = df_defined.Filter(runnum_cut1).Histo1D(('MSeg_etaPos','MSeg eta',50,-2.5,2.5),'MSeg_etaPos')
    h_nMSTracklet = df_defined.Filter(runnum_cut1).Histo1D(('nMSTracklet','nMSTracklet',50,0.,50.),'nMSTracklet')
    h_MSTracklet_eta = df_defined.Filter(runnum_cut1).Histo1D(('MSTracklet_eta','MSTracklet eta',50,-2.5,2.5),'MSTracklet_eta')

    h_nMSVtx_hits = df_defined.Filter(runnum_cut1).Histo1D(('nMSVtx_hits','n MSVtx with hits cuts',10,0.,10.),'nMSVtx_hits')
    h_MSVtx_hits_eta = df_defined.Filter(runnum_cut1).Histo1D(('MSVtx_hits_eta','n MSVtx eta with hits cuts',50,-2.5,2.5),'MSVtx_hits_eta')
    h_MSVtx_barrel_hits_R = df_defined.Histo1D(('nMSVtx_barrel_hits_R','MSVtx R with barrel hits cuts',50,0.,15000.),'MSVtx_barrel_hits_R')
    h_MSVtx_barrel_hits_z = df_defined.Histo1D(('nMSVtx_barrel_hits_z','MSVtx z with barrel hits cuts',50,0.,15000.),'MSVtx_barrel_hits_z')
    h_MSVtx_barrel_hits_3dradius = df_defined.Histo1D(('nMSVtx_barrel_hits_3dradius','MSVtx 3dradius with barrel hits cuts',50,0.,15000.),'MSVtx_barrel_hits_3dradius')
    h_MSVtx_barrel_hits_jet_drmin = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_barrel_cut1).Histo1D(('MSVtx_barrel_hits_jet_drmin','closest drmin of jets for barrel 1 MSVtx',50,0.,2.),\
'MSVtx_barrel_hits_vec_jet_drmin','jetsliceweight')
    h_MSVtx_barrel_hits_jet_pt = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_barrel_cut1).Histo1D(('MSVtx_barrel_hits_jet_pt','closest jet pt for barrel 1 MSVtx',50,0.,2500.),\
'MSVtx_barrel_hits_vec_jet_pt','jetsliceweight')
    h_MSVtx_barrel_hits_jet_logratio = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_barrel_cut1).Histo1D(('MSVtx_barrel_hits_jet_logratio','closest jet logratio for barrel 1 MSVtx',50,0.,2.),\
'MSVtx_barrel_hits_vec_jet_lograt','jetsliceweight')
    h_MSVtx_barrel_hits_jet_hecf = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_barrel_cut1).Histo1D(('MSVtx_barrel_hits_jet_hecf','closest jet hecf for barrel 1 MSVtx',50,0.,1.),\
'MSVtx_barrel_hits_vec_jet_hecf','jetsliceweight')
    h_MSVtx_barrel_hits_jet_nmdt = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_barrel_cut1).Histo1D(('MSVtx_barrel_hits_jet_nmdt','nmdt for barrel 1 MSVtx closest jet',50,0.,4000.),\
'MSVtx_barrel_hits_vec_jet_nmdt','jetsliceweight')
    h_MSVtx_barrel_hits_jet_nrpc = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_barrel_cut1).Histo1D(('MSVtx_barrel_hits_jet_nrpc','nmdt for barrel 1 MSVtx closest jet',50,0.,4000.),\
'MSVtx_barrel_hits_vec_jet_nrpc','jetsliceweight')
    h_MSVtx_barrel_hits_nMSeg_eta = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_barrel_cut1).Histo1D(('MSVtx_barrel_hits_nMSeg_eta','nMSeg in dR for barrel 1 MSVtx',50,0.,500.),\
'MSVtx_barrel_hits_nMSeg_eta','jetsliceweight')
    h_MSVtx_barrel_hits_nMSTracklet_eta = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_barrel_cut1).Histo1D(('MSVtx_barrel_hits_nMSTracklet_eta','nMSTracklet in dR for barrel 1 MSVtx',\
50,0.,500.),\
'MSVtx_barrel_hits_nMSTracklet_eta','jetsliceweight')
    h_MSVtx_barrel_hits_track_drmin = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_barrel_cut1).Histo1D(('MSVtx_barrel_hits_track_drmin','track 5000 drmin for barrel 1 MSVtx',\
50,0.,2.),\
'MSVtx_barrel_hits_track_drmin','jetsliceweight')
    h_MSVtx_barrel_hits_dr_mean_MSeg = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_barrel_cut1).Histo1D(('MSVtx_barrel_hits_dr_mean_MSeg','MSeg dr mean for barrel 1 MSVtx',50,0.,2.),\
'MSVtx_barrel_hits_dr_mean_MSeg','jetsliceweight')
    h_MSVtx_barrel_hits_dr_rms_MSeg = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_barrel_cut1).Histo1D(('MSVtx_barrel_hits_dr_rms_MSeg','MSeg dr rms for barrel 1 MSVtx',50,0.,2.),\
'MSVtx_barrel_hits_dr_rms_MSeg','jetsliceweight')
    h_MSVtx_barrel_hits_dr_mean_MSTracklet = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_barrel_cut1).Histo1D(('MSVtx_barrel_hits_dr_mean_MSTracklet','MSTracklet dr \
mean for barrel 1 MSVtx',50,0.,2.),\
'MSVtx_barrel_hits_dr_mean_MSTracklet','jetsliceweight')
    h_MSVtx_barrel_hits_dr_rms_MSTracklet = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_barrel_cut1).Histo1D(('MSVtx_barrel_hits_dr_rms_MSTracklet','MSTracklet dr \
rms for barrel 1 MSVtx',50,0.,2.),\
'MSVtx_barrel_hits_dr_rms_MSTracklet','jetsliceweight')
    h_MSVtx_endcap_hits_R = df_defined.Histo1D(('nMSVtx_endcap_hits_R','MSVtx R with endcap hits cuts',50,0.,15000.),'MSVtx_endcap_hits_R')
    h_MSVtx_endcap_hits_z = df_defined.Histo1D(('nMSVtx_endcap_hits_z','MSVtx z with endcap hits cuts',50,0.,15000.),'MSVtx_endcap_hits_z')
    h_MSVtx_endcap_hits_3dradius = df_defined.Histo1D(('nMSVtx_endcap_hits_3dradius','MSVtx 3dradius with endcap hits cuts',50,0.,15000.),'MSVtx_endcap_hits_3dradius')
    h_MSVtx_endcap_hits_jet_drmin = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_endcap_cut1).Histo1D(('MSVtx_endcap_hits_jet_drmin','closest drmin of jets for endcap 1 MSVtx',50,0.,2.),\
'MSVtx_endcap_hits_vec_jet_drmin','jetsliceweight')
    h_MSVtx_endcap_hits_jet_pt = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_endcap_cut1).Histo1D(('MSVtx_endcap_hits_jet_pt','closest jet pt for endcap 1 MSVtx',50,0.,2500.),\
'MSVtx_endcap_hits_vec_jet_pt','jetsliceweight')
    h_MSVtx_endcap_hits_jet_logratio = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_endcap_cut1).Histo1D(('MSVtx_endcap_hits_jet_logratio','closest jet logratio for endcap 1 MSVtx',50,0.,2.),\
'MSVtx_endcap_hits_vec_jet_lograt','jetsliceweight')
    h_MSVtx_endcap_hits_jet_hecf = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_endcap_cut1).Histo1D(('MSVtx_endcap_hits_jet_hecf','closest jet hecf for endcap 1 MSVtx',50,0.,1.),\
'MSVtx_endcap_hits_vec_jet_hecf','jetsliceweight')
    h_MSVtx_endcap_hits_jet_nmdt = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_endcap_cut1).Histo1D(('MSVtx_endcap_hits_jet_nmdt','nmdt for endcap 1 MSVtx closest jet',50,0.,4000.),\
'MSVtx_endcap_hits_vec_jet_nmdt')
    h_MSVtx_endcap_hits_nMSeg_eta = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_endcap_cut1).Histo1D(('MSVtx_endcap_hits_nMSeg_eta','nMSeg in dR for endcap 1 MSVtx',50,0.,500.),\
'MSVtx_endcap_hits_nMSeg_eta')
    h_MSVtx_endcap_hits_nMSTracklet_eta = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_endcap_cut1).Histo1D(('MSVtx_endcap_hits_nMSTracklet_eta','nMSTracklet in dR for endcap 1 MSVtx',\
50,0.,500.),\
'MSVtx_endcap_hits_nMSTracklet_eta')
    h_MSVtx_endcap_hits_track_drmin = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_endcap_cut1).Histo1D(('MSVtx_endcap_hits_track_drmin','track 5000 drmin for endcap 1 MSVtx',50,0.,2.),\
'MSVtx_endcap_hits_track_drmin')
    h_MSVtx_endcap_hits_dr_mean_MSeg = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_endcap_cut1).Histo1D(('MSVtx_endcap_hits_dr_mean_MSeg','MSeg dr mean for endcap 1 MSVtx',50,0.,2.),\
'MSVtx_endcap_hits_dr_mean_MSeg')
    h_MSVtx_endcap_hits_dr_rms_MSeg = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_endcap_cut1).Histo1D(('MSVtx_endcap_hits_dr_rms_MSeg','MSeg dr rms for endcap 1 MSVtx',50,0.,2.),\
'MSVtx_endcap_hits_dr_rms_MSeg')
    h_MSVtx_endcap_hits_dr_mean_MSTracklet = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_endcap_cut1).Histo1D(('MSVtx_endcap_hits_dr_mean_MSTracklet','MSTracklet dr \
mean for endcap 1 MSVtx',50,0.,2.),\
'MSVtx_endcap_hits_dr_mean_MSTracklet')
    h_MSVtx_endcap_hits_dr_rms_MSTracklet = df_defined.Filter(runnum_cut1).Filter(clean_cut1).Filter(msvtx_endcap_cut1).Histo1D(('MSVtx_endcap_hits_dr_rms_MSTracklet','MSTracklet dr \
rms for endcap 1 MSVtx',50,0.,2.),\
'MSVtx_endcap_hits_dr_rms_MSTracklet')


if (datatype == 2):
  h_llp_dR = df_defined.Histo1D(('llp_dR','llp dR (no cuts)',50,0.,3.5),'llp_DeltaR')
  h_llp_Lxy = df_defined_llp2.Histo1D(('llp_Lxy','LLP_Lxy',50,0.,5000.),'llp_Lxy')
  h_llp_Lz = df_defined_llp2.Histo1D(('llp_Lz','LLP_Lz',50,0.,15000.),'llp_Lz')
  h_llp_Lr = df_defined_llp2.Histo1D(('llp_Lr','LLP_Lr',50,0.,15000.),'llp_Lr')
  h_MSVtx_hits_llp_drmin = df_defined_llp2.Filter(msvtx_cut1).Histo1D(('MSVtx_hits_llp_drmin','closest drmin of jets for 1 MSVtx',50,0.,2.),\
'MSVtx_hits_vec_llp_drmin')
  h_MSVtx_hits_llp_Lxy = df_defined_llp2.Filter(msvtx_cut1).Histo1D(('MSVtx_hits_llp_Lxy','closest llp Lxy for 1 MSVtx',50,0.,15000.),\
'MSVtx_hits_vec_llp_Lxy')
  h_MSVtx_hits_llp_Lz = df_defined_llp2.Filter(msvtx_cut1).Histo1D(('MSVtx_hits_llp_Lz','closest llp Lz for 1 MSVtx',50,0.,15000.),\
'MSVtx_hits_vec_llp_Lz')
  h_MSVtx_hits_llp_nmdt = df_defined_llp2.Filter(msvtx_cut1).Histo1D(('MSVtx_hits_llp_nmdt','nmdt for 1 MSVtx closest llp',50,0.,4000.),\
'MSVtx_hits_vec_llp_nmdt')
  h_LLP_MSVtx_dxy =  df_defined_llp2.Filter(msvtx_cut1).Histo1D(('LLP_MSVtx_dxy','LLP - MSVtx dxy',50,0.,5000.),\
'LLP_MSVtx_dxy')
  h_LLP_MSVtx_dz =  df_defined_llp2.Filter(msvtx_cut1).Histo1D(('LLP_MSVtx_dz','LLP - MSVtx dz',50,0.,5000.),\
'LLP_MSVtx_dz')

if (datatype == 3 or datatype == 20 or datatype == 10):
    ROOT.TH1.SetDefaultSumw2(ROOT.kTRUE)
    h_NPV = df_defined.Filter(clean_cut1).Filter(met_cut1).Histo1D(('NPV','NPV',100,0.,100.),'NPV','jetsliceweight')
    h_njets = df_defined.Filter(clean_cut1).Filter(met_cut1).Histo1D(('njets','n jets',50,0.,50.),'njets','jetsliceweight')
    h_njets_noweight = df_defined.Filter(clean_cut1).Filter(met_cut1).Histo1D(('njets_noweight','njets noweight',100,0.,100.),'njets')
    h_njets_noweight_barrel = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('njets_noweight_barrel','njets noweight barrel',100,0.,100.),'njets')
    h_njets_noweight_endcap = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_cut1).Histo1D(('njets_noweight_endcap','njets noweight endcap',100,0.,100.),'njets')
    h_njets_njets_barrel = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('njets_njets_barrel','n jets njets barrel',50,0.,50.),'njets','njets_weighted')
    h_njets_njets_endcap = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_cut1).Histo1D(('njets_njets_endcap','n jets njets endcap',50,0.,50.),'njets','njets_weighted')
    h_leading_jet_pt_idx = df_defined.Filter(clean_cut1).Filter(met_cut1).Histo1D(('leading_jet_pt_idx','leading jet pt idx',20,0.,20.),'leading_jet_pt_idx','jetsliceweight')
    h_log10_jetsliceweight = df_defined.Filter(clean_cut1).Filter(met_cut1).Histo1D(('log10_jetsliceweight','log10 jetsliceweight',20,0.,20.),'log10_jetsliceweight')
    h_jetsliceweight = df_defined.Filter(clean_cut1).Filter(met_cut1).Histo1D(('jetsliceweight','jetsliceweight',100,0.,10000.),'jetsliceweight')
    h_jet_pt_GeV = df_defined.Filter(clean_cut1).Filter(met_cut1).Histo1D(('jet_pt','jet pt GeV',50,0.,1000.),'jet_pt_GeV','jetsliceweight')
    h_jet_eta = df_defined.Filter(clean_cut1).Filter(met_cut1).Histo1D(('jet_eta_weighted','jet eta weighted',50,-5.,5.),'jet_eta','jetsliceweight')
    h_jet_lograt = df_defined.Filter(clean_cut1).Filter(met_cut1).Histo1D(('jet_lograt','jet lograt',50,-5.,5.),'jet_logRatio','jetsliceweight')
    h_jet_HECF = df_defined.Filter(clean_cut1).Filter(met_cut1).Histo1D(('jet_HECF','jet HECF',50,0.,1.),'jet_HECF','jetsliceweight')
    h_met_GeV = df_defined.Filter(clean_cut1).Filter(met_cut1).Histo1D(('met_GeV','met GeV',50,0.,100.),'met_GeV','jetsliceweight')
    h_nMSVtx =  df_defined.Filter(clean_cut1).Filter(met_cut1).Histo1D(('nMSVtx','nMSVtx',5,0.,5.),'nMSVtx','jetsliceweight')
    h_nMSVtx_hits =  df_defined.Filter(clean_cut1).Filter(met_cut1).Histo1D(('nMSVtx_hits','nMSVtx hits',5,0.,5.),'nMSVtx_hits','jetsliceweight')   

# barrel
    h_nMSVtx_barrel_hits =  df_defined.Filter(clean_cut1).Filter(met_cut1).Histo1D(('nMSVtx_barrel_hits','nMSVtx barrel hits',5,0.,5.),'nMSVtx_barrel_hits','jetsliceweight')
    h_MSVtx_barrel_hits_vec_jet_drmin = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('MSVtx_barrel_hits_vec_jet_drmin',\
'closest drmin of jets for barrel 1 MSVtx hits',50,0.,2.),'MSVtx_barrel_hits_vec_jet_drmin','jetsliceweight')
    h_MSVtx_barrel_hits_vec_jet_pt = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('MSVtx_barrel_hits_vec_jet_pt',\
'closest jet pt for barrel 1 MSVtx hits',50,0.,2000.),'MSVtx_barrel_hits_vec_jet_pt','jetsliceweight')
    h_MSVtx_barrel_hits_vec_jet_idx = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('MSVtx_barrel_hits_vec_jet_idx',\
'closest jet idx for barrel 1 MSVtx hits',20,0.,20.),'MSVtx_barrel_hits_vec_jet_idx','jetsliceweight')
    h_MSVtx_barrel_hits_vec_jet_nmdt = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('MSVtx_barrel_hits_vec_jet_nmdt',\
'nmdt for barrel 1 MSVtx hits',50,0.,5000.),'MSVtx_barrel_hits_vec_jet_nmdt','jetsliceweight')
    h_MSVtx_barrel_hits_vec_jet_nrpc = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('MSVtx_barrel_hits_vec_jet_nrpc',\
'nrpc for barrel 1 MSVtx hits',50,0.,5000.),'MSVtx_barrel_hits_vec_jet_nrpc','jetsliceweight')
    h_MSVtx_barrel_hits_nMSeg_eta =  df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('MSVtx_barrel_hits_nMSeg_eta',\
'nMSeg for barrel 1 MSVtx hits',50,0.,500.),'MSVtx_barrel_hits_nMSeg_eta','jetsliceweight')
    h_MSVtx_barrel_hits_nMSTracklet_eta =  df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('MSVtx_barrel_hits_nMSTracklet_eta',\
'nMSTracklet for barrel 1 MSVtx hits',50,0.,500.),'MSVtx_barrel_hits_nMSTracklet_eta','jetsliceweight')
    h_MSVtx_barrel_hits_g5track_drmin =  df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('MSVtx_barrel_hits_g5track_drmin',\
'g5track drmin for barrel 1 MSVtx hits',50,0.,2.),'MSVtx_barrel_hits_g5track_drmin','jetsliceweight')
    h_MSVtx_barrel_hits_g5track_ptsum_opposite =  df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('MSVtx_barrel_hits_g5track_ptsum_opposite',\
'g5track ptsum opposite for barrel 1 MSVtx hits',50,0.,500.),'MSVtx_barrel_hits_g5track_ptsum_opposite','jetsliceweight')
    h_MSVtx_barrel_hits_dr_mean_MSeg = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('MSVtx_barrel_hits_dr_mean_MSeg',\
'MSeg dr mean for barrel 1 MSVtx',50,0.,2.),\
'MSVtx_barrel_hits_dr_mean_MSeg','jetsliceweight')
    h_MSVtx_barrel_hits_dr_rms_MSeg = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('MSVtx_barrel_hits_dr_rms_MSeg',\
'MSeg dr rms for barrel 1 MSVtx',50,0.,2.),\
'MSVtx_barrel_hits_dr_rms_MSeg','jetsliceweight')
    h_MSVtx_barrel_hits_dr_mean_MSTracklet = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('MSVtx_barrel_hits_dr_mean_MSTracklet',\
'MSTracklet dr mean for barrel 1 MSVtx',50,0.,2.),\
'MSVtx_barrel_hits_dr_mean_MSTracklet','jetsliceweight')
    h_MSVtx_barrel_hits_dr_rms_MSTracklet = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('MSVtx_barrel_hits_dr_rms_MSTracklet',\
'MSTracklet dr rms for barrel 1 MSVtx',50,0.,2.),\
'MSVtx_barrel_hits_dr_rms_MSTracklet','jetsliceweight')
    h_MSVtx_barrel_hits_met_dphi = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('MSVtx_barrel_hits_met_dphi',\
'MSVtx_barrel_hits_met_dphi',50,0.,3.2),'MSVtx_barrel_hits_met_dphi','jetsliceweight')
# endcap 
    h_nMSVtx_endcap_hits =  df_defined.Filter(clean_cut1).Filter(met_cut1).Histo1D(('nMSVtx_endcap_hits','nMSVtx endcap hits',5,0.,5.),'nMSVtx_endcap_hits','jetsliceweight')
    h_MSVtx_endcap_hits_vec_jet_drmin = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_cut1).Histo1D(('MSVtx_endcap_hits_vec_jet_drmin',\
'closest drmin of jets for endcap 1 MSVtx hits',50,0.,2.),'MSVtx_endcap_hits_vec_jet_drmin','jetsliceweight')
    h_MSVtx_endcap_hits_vec_jet_pt = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_cut1).Histo1D(('MSVtx_barrel_hits_vec_jet_pt',\
'closest jet pt for endcap 1 MSVtx hits',50,0.,2000.),'MSVtx_endcap_hits_vec_jet_pt','jetsliceweight')
    h_MSVtx_endcap_hits_vec_jet_idx = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_cut1).Histo1D(('MSVtx_endcap_hits_vec_jet_idx',\
'closest jet idx for endcap 1 MSVtx hits',20,0.,20.),'MSVtx_endcap_hits_vec_jet_idx','jetsliceweight')
    h_MSVtx_endcap_hits_vec_jet_nmdt = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_cut1).Histo1D(('MSVtx_endcap_hits_vec_jet_nmdt',\
'nmdt for endcap 1 MSVtx hits',50,0.,5000.),'MSVtx_endcap_hits_vec_jet_nmdt','jetsliceweight')
    h_MSVtx_endcap_hits_vec_jet_ntgc = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_cut1).Histo1D(('MSVtx_endcap_hits_vec_jet_ntgc',\
'tgc for endcap 1 MSVtx hits',50,0.,5000.),'MSVtx_endcap_hits_vec_jet_ntgc','jetsliceweight')
    h_MSVtx_endcap_hits_nMSeg_eta =  df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_cut1).Histo1D(('MSVtx_endcap_hits_nMSeg_eta',\
'nMSeg for endcap 1 MSVtx hits',50,0.,500.),'MSVtx_endcap_hits_nMSeg_eta','jetsliceweight')
    h_MSVtx_endcap_hits_nMSTracklet_eta =  df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_cut1).Histo1D(('MSVtx_endcap_hits_nMSTracklet_eta',\
'nMSTracklet for endcap 1 MSVtx hits',50,0.,500.),'MSVtx_endcap_hits_nMSTracklet_eta','jetsliceweight')
    h_MSVtx_endcap_hits_g5track_drmin =  df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_cut1).Histo1D(('MSVtx_endcap_hits_g5track_drmin',\
'g5track drmin for endcap 1 MSVtx hits',50,0.,2.),\
'MSVtx_endcap_hits_g5track_drmin','jetsliceweight')
    h_MSVtx_endcap_hits_g5track_ptsum_opposite =  df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Histo1D(('MSVtx_endcap_hits_g5track_ptsum_opposite_side',\
'g5track ptsum opposite for endcap 1 MSVtx hits',50,0.,200.),'MSVtx_endcap_hits_g5track_ptsum_opposite','jetsliceweight')
    h_MSVtx_endcap_hits_dr_mean_MSeg = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_cut1).Histo1D(('MSVtx_endcap_hits_dr_rms_MSeg',\
'MSeg dr rms for endcap 1 MSVtx',50,0.,2.),\
'MSVtx_endcap_hits_dr_mean_MSeg','jetsliceweight')
    h_MSVtx_endcap_hits_dr_rms_MSeg = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_cut1).Histo1D(('MSVtx_endcap_hits_dr_rms_MSeg',\
'MSeg dr rms for endcap 1 MSVtx',50,0.,2.),\
'MSVtx_endcap_hits_dr_rms_MSeg','jetsliceweight')
    h_MSVtx_endcap_hits_dr_mean_MSTracklet = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_cut1).Histo1D(('MSVtx_endcap_hits_dr_mean_MSTracklet',\
'MSTracklet dr mean for endcap 1 MSVtx',50,0.,2.),\
'MSVtx_endcap_hits_dr_mean_MSTracklet','jetsliceweight')
    h_MSVtx_endcap_hits_dr_rms_MSTracklet = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_cut1).Histo1D(('MSVtx_endcap_hits_dr_rms_MSTracklet',\
'MSTracklet dr rms for endcap 1 MSVtx',50,0.,2.),\
'MSVtx_endcap_hits_dr_rms_MSTracklet','jetsliceweight')
    h_MSVtx_endcap_hits_met_dphi = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_cut1).Histo1D(('MSVtx_endcap_hits_met_dphi',\
'MSVtx_endcap_hits_met_dphi',50,0.,3.2),'MSVtx_endcap_hits_met_dphi','jetsliceweight')

print ('step 5')

#h_logratio_njets = df_defined.Histo1D(('logratio_njets','logratio njets',20,0.,20.),'logratio_njets')
#h_logratio_jet_pt = df_defined.Histo1D(('logratio_jet_pt','logratio jet pt',50,0.,1000.),'logratio_jet_pt')
#h_leading_jet_pt = df_defined.Histo1D(('leading_jet_pt','leading jet pt',50,0.,2000.),'leading_jet_pt')
#h_leading_jet_pt_weighted = df_defined.Histo1D(('leading_jet_pt weighted','leading jet pt weighted',50,0.,2000.),'leading_jet_pt','jetsliceweight')
#h_leading_logratio_jet_pt = df_defined.Histo1D(('leading_logratio_jet_pt','leading logratio jet pt',50,0.,500.),'leading_logratio_jet_pt')
#h_MSTracklet_pT_jet_match = df_defined.Histo1D(('MSTracklet_pT_jet_match','MSTracklet pT leading jet match',50,0.,50000.),'MSTracklet_pT_jet_match')
#h_MSTracklet_pT_logratio_jet_match = df_defined.Histo1D(('MSTracklet_pT_logratio_jet_match','MSTracklet pT leading logratio jet match',50,0.,50000.),'#MSTracklet_pT_logratio_jet_match')
#h_nMSTracklet_pT_jet_match = df_defined.Histo1D(('nMSTracklet_pT_jet_match','nMSTracklet leading jet match',20,0.,20.),'nMSTracklet_pT_jet_match')
#h_nMSTracklet_pT_logratio_jet_match = df_defined.Histo1D(('nMSTracklet_pT_logratio jet_match','nMSTracklet leading logratio jet match',20,0.,20.),'nMS#Tracklet_pT_logratio_jet_match')
#h_nMSeg_etaPos_jet_match = df_defined.Histo1D(('nMSeg_etaPos_jet_match','nMSeg leading jet_match',50,0.,1000.),'nMSeg_etaPos_jet_match')
#h_nMSeg_etaPos_logratio_jet_match = df_defined.Histo1D(('nMSeg_etaPos_logratio_jet_match','nMSeg leading logratio jet match',50,0.,1000.),'nMSeg_etaPo#s_logratio_jet_match')
#h2_nMSTracklet_pT_leading_jet_match = df_defined.Histo2D(('nMSTracklet_pT_leading_jet_match','nMSTracklet versus leading jet pT',50,0.,2000.,20,0.,40.#),'leading_jet_pt','nMSTracklet_pT_jet_match')
#h2_nMSTracklet_pT_leading_logratio_jet_match =df_defined.Histo2D(('nMSTracklet_pT_leading_logratio_jet_match','nMSTracklet versus leading logratio jet# pT',50,0.,500.,20,0.,40.),'leading_logratio_jet_pt','nMSTracklet_pT_logratio_jet_match')
#h2_nMSeg_etaPos_leading_jet_match =df_defined.Histo2D(('nMSeg_etaPos_leading_jet_match','nMSeg versus leading jet pT',50,0.,2000.,20,0.,200.),'leading#_jet_pt','nMSeg_etaPos_jet_match')
#h2_nMSeg_etaPos_leading_logratio_jet_match =df_defined.Histo2D(('nMSeg_etaPos_leading_logratio_jet_match','nMSeg versus leading logratio jet pT',50,0.#,200.,20,0.,200.),'leading_logratio_jet_pt','nMSeg_etaPos_logratio_jet_match')
#h_nMSTracklet = df_defined.Histo1D(('nMSTracklet','n MSTracklet',50,0.,50.),'nMSTracklet')
#h_nMSeg = df_defined.Histo1D(('nMSeg','n MSeg',50,0.,2000.),'nMSeg')
#h_nmuRoIClus = df_defined.Histo1D(('nmuRoIClus','n muRoIClus',20,0.,20.),'nmuRoIClus')
#h_nMSVtx = df_defined.Histo1D(('nMSVtx','n msvtx',20,0.,20.),'nMSVtx') 
#h_nMSVtx_barrel = df_defined.Histo1D(('nMSVtx_barrel','n msvtx barrel',20,0.,20.),'nMSVtx_barrel')
#h_nMSVtx_barrel_hits = df_defined.Histo1D(('nMSVtx_barrel_hits','n msvtx barrel hits',20,0.,20.),'nMSVtx_barrel_hits')
#h_nMSVtx_iso_hits = df_defined.Histo1D(('nMSVtx_iso_hits','nMSVtx iso hits',20,0.,20.),'nMSVtx_iso_hits')
#h_njets_loose_cluster = df_defined.Histo1D(('njets_loose_cluster','njets_loose_cluster',20,0.,20.),'njets_loose_cluster')
#h_nMSVtx_hits =   df_defined.Histo1D(('nMSVtx_hits','nMSVtx with hits selection',20,0.,20.),'nMSVtx_hits')
#h_MSVtx_hits_jet_drmin = df_defined.Filter(msvtx_cut1).Histo1D(('MSVtx_hits_jet_drmin','closest drmin of jets for 1 MSVtx',50,0.,2.),'MSVtx_hits_vec_d#rmin','jetsliceweight')
#h_MSVtx_hits_jet_pt = df_defined.Filter(msvtx_cut1).Histo1D(('MSVtx_hits_jet_pt','closest jet pt for 1 MSVtx',50,0.,2000.),'MSVtx_hits_vec_pt','jetsli#ceweight')
#h_MSVtx_hits_jet_logratio = df_defined.Filter(msvtx_cut1).Histo1D(('MSVtx_hits_jet_logratio','closest jet logratio for 1 MSVtx',50,0.,2.),'MSVtx_hits_#vec_lograt','jetsliceweight')
#h_MSVtx_hits_jet_nmdt = df_defined.Filter(msvtx_cut1).Histo1D(('MSVtx_hits_jet_nmdt','nmdt for 1 MSVt\
#x closest jet',50,0.,4000.),'MSVtx_hits_vec_nmdt','jetsliceweight')

# msvtx
#h_nmsvtx = df_defined.Histo1D(('nmsvtx','n mstvx',20,0.,20.),'nmsvtx')
#h_msvtx_closejetdr = df_defined.Histo1D(('msvtx_closejetdr','msvtx closejetdr',100,0.,4.),'MSVtx_closestJetdR')
#h_msvtx_hightrackdr = df_defined.Histo1D(('msvtx_hightrackdr','msvtx_hightrackdr',100,0.,4.),'MSVtx_closestHighPtTrackdR')
#h_msvtx_lowtrackdr = df_defined.Histo1D(('msvtx_lowtrackdr','msvtx lowtrackdr',100,0.,4.),'MSVtx_closestLowPtTrackdR')
#h_msvtx_nmdt = df_defined.Histo1D(('msvtx_nmdt','msvtx nmdt',100,0.,5000.),'MSVtx_nMDT')
#h_msvtx_sumtrackptcone = df_defined.Histo1D(('msvtx_sumtrackptcone','msvtx sumtrackptcone',100,0.,30000.),'MSVtx_sumTrackPt0p2Cone')


# all events
eventNumber_cut0 = 'NPV > -1'
entries = df_defined.Filter(eventNumber_cut0).Count()
print("%s total entries" %entries.GetValue())

dofilter = 0
print ('dofilter = ',dofilter)
if (dofilter == 1):

# all events with 1 barrel mxvtx hits
    entries = df_defined.Filter(msvtx_barrel_hits_cut1).Count()
    print("%s msvtx_barrel_hits_cut1" %entries.GetValue())
    entries = df_defined.Filter(msvtx_endcap_hits_cut1).Count()
    print("%s msvtx_endcap_hits_cut1" %entries.GetValue())
    entries = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_cut1).Count()
    print("%s clean_cut1 and met cut and msvtx_barrel_hits_cut1" %entries.GetValue())
    entries = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_cut1).Count()
    print("%s clean cut1 and met cut and msvtx_endcap_hits_cut1" %entries.GetValue())

# roi trigger
    muon_roi_cluster_trigger_cut0 = 'pass_HLT_j30_muvtx_noiso == 1'
    entries = df.Filter(muon_roi_cluster_trigger_cut0).Count()
    print("%s entries passed cluster trigger cut" %entries.GetValue())

# pv tracks
    pv_tracks_cut0 = 'ntracks_pt400 > 1'
    entries = df_defined.Filter(pv_tracks_cut0).Count() 
    print("%s entries passed pv track cut" %entries.GetValue())

# msvtx initial cut
    msvtx_cut0 = 'nMSVtx > 0'
    entries = df_defined.Filter(msvtx_cut0).Count()
    print("%s entries with at least one msvtx" %entries.GetValue())

# msvtx only one
    msvtx_cut1 = 'nMSVtx_hits == 1'

# vtx hits
    df_vtx = df.Define('pass_ms_vertex_hits','hits(MSVtx_eta,MSVtx_nMDT,MSVtx_nRP  C,MSVtx_nTGC)')
    msvtx_hit_cut0 = 'pass_ms_vertex_hits == 1'
    entries = df_vtx.Filter(msvtx_hit_cut0).Count()
    print("%s entries passed one ms vertex hits cut" %entries.GetValue())

# pv cut
    pv_cut0 = 'goodPV'
    entries = df_defined.Filter(pv_cut0).Count()
    print("%s entries passed at least one good PV" %entries.GetValue())

# pv cut
    pv_cut1 = 'hasGoodPV == 1'
    entries = df.Filter(pv_cut1).Count()
    print("%s entries passed at least one good PV" %entries.GetValue())

# cleaning flags
    clean_cut0 = 'isClean'
    entries = df_defined.Filter(clean_cut0).Count()
    print("%s entries passed all cleaning flags (except JetClean)" %entries.GetValue())

# barrel cluster
    barrel_clusters_cut1 = 'muRoIClus_barrel_eta.size() == 1'
    entries = df_defined.Filter(barrel_clusters_cut1).Count()
    print("%s entries passed barrel muroi clusters == 1" %entries.GetValue())

# barrel cluster
    barrel_clusters_cut2 = 'muRoIClus_barrel_eta.size() == 2'
    entries = df_defined.Filter(barrel_clusters_cut2).Count()
    print("%s entries passed barrel muroi clusters == 2" %entries.GetValue())

# endcap cluster
    endcap_clusters_cut1 = 'muRoIClus_endcap_eta.size() == 1'
    entries = df_defined.Filter(endcap_clusters_cut1).Count()
    print("%s entries passed endcap muroi clusters == 1" %entries.GetValue())

# endcap cluster
    endcap_clusters_cut2 = 'muRoIClus_endcap_eta.size() == 2'
    entries = df_defined.Filter(endcap_clusters_cut2).Count()
    print("%s entries passed endcap muroi clusters == 2" %entries.GetValue())


# make cuts
    isClean = df_defined.Filter('isClean')
    goodPV = isClean.Filter('hasGoodPV')
    goodTrigger = goodPV.Filter('pass_HLT_j30_muvtx_noiso == 1')


#barrel_Cluster1 = goodTrigger.Filter('muRoIClus_barrel_eta.size() == 1 && nmuRoIClus == 1')
#entries = barrel_Cluster1.Count()
#print("%s entries passed barrel muroi clusters == 1" %entries.GetValue())
#barrel_Cluster1JetIso = barrel_Cluster1.Filter('var1_var2_match1(0.3,muRoIClus_barrel_eta,muRoIClus_barrel_phi,\
#loose_cluster_jets_eta,loose_cluster_jets_phi) == 0')
#entries = barrel_Cluster1JetIso.Count()
#print("%s entries passed jet isolated barrel muroi clusters == 1" %entries.GetValue())
#barrel_Cluster1TrackIso = barrel_Cluster1JetIso.Filter('var1_var2_match1(0.3,muRoIClus_barrel_eta,muRoIClus_barrel_phi,\
#track_g5_eta,track_g5_phi) == 0')
#entries = barrel_Cluster1TrackIso.Count()
#print("%s entries passed track isolated barrel muroi clusters == 1" %entries.GetValue())
#barrel_Cluster1TrackSumIso = barrel_Cluster1TrackIso.Filter('var1_var2_ptsum(0.2,muRoIClus_barrel_eta,\
#muRoIClus_barrel_phi, track_l5_eta, track_l5_phi, track_l5_pt) < 10000.')
#entries = barrel_Cluster1TrackSumIso.Count()
#print("%s entries passed track sum isolated barrel muroi clusters == 1" %entries.GetValue())

#endcap_Cluster1 = goodTrigger.Filter('muRoIClus_endcap_eta.size() == 1 && nmuRoIClus == 1')
#entries = endcap_Cluster1.Count()
#print("%s entries passed endcap muroi clusters == 1" %entries.GetValue())
#endcap_Cluster1JetIso = endcap_Cluster1.Filter('var1_var2_match1(0.6,muRoIClus_endcap_eta,muRoIClus_endcap_phi,\
#loose_cluster_jets_eta,loose_cluster_jets_phi) == 0')
#entries = endcap_Cluster1JetIso.Count()
#print("%s entries passed jet isolated endcap muroi clusters == 1" %entries.GetValue())
#endcap_Cluster1TrackIso = endcap_Cluster1JetIso.Filter('var1_var2_match1(0.6,muRoIClus_endcap_eta,muRoIClus_endcap_phi,\
#track_g5_eta,track_g5_phi) == 0')
#entries = endcap_Cluster1TrackIso.Count()
#print("%s entries passed track isolated endcap muroi clusters == 1" %entries.GetValue())
#endcap_Cluster1TrackSumIso = endcap_Cluster1TrackIso.Filter('var1_var2_ptsum(0.2,muRoIClus_endcap_eta,\
#muRoIClus_endcap_phi, track_l5_eta, track_l5_phi, track_l5_pt) < 10000.')
#entries = endcap_Cluster1TrackSumIso.Count()
#print("%s entries passed track sum isolated endcap muroi clusters == 1" %entries.GetValue())


#barrel_MSVtx1 = goodTrigger.Filter('MSVtx_barrel_hits_eta.size() == 1 && nMSVtx == 1')
#entries = barrel_MSVtx1.Count()
#print("%s entries passed msvtx barrel hits cut == 1" %entries.GetValue())
#barrel_MSVtx1JetIso = barrel_MSVtx1.Filter('var1_var2_match1(0.3,MSVtx_barrel_hits_eta,MSVtx_barrel_hits_phi,\
#loose_cluster_jets_eta,loose_cluster_jets_phi) == 0')
#entries = barrel_MSVtx1JetIso.Count()
#print("%s entries passed jet isolated msvtx barrel hits cut == 1" %entries.GetValue())
#barrel_MSVtx1TrackIso = barrel_MSVtx1JetIso.Filter('var1_var2_match1(0.3,MSVtx_barrel_hits_eta,MSVtx_barrel_hits_phi,\
#track_g5_eta,track_g5_phi) == 0')
#entries = barrel_MSVtx1TrackIso.Count()
#print("%s entries passed track isolated msvtx barrel hits cut == 1" %entries.GetValue())
#barrel_MSVtx1TrackSumIso = barrel_MSVtx1TrackIso.Filter('var1_var2_ptsum(0.2,MSVtx_barrel_hits_eta,MSVtx_barrel_hits_phi,\
#track_l5_eta,track_l5_phi, track_l5_pt) < 10000.')
#entries = barrel_MSVtx1TrackSumIso.Count()
#print("%s entries passed track sum isolated msvtx barrel hits cut == 1" %entries.GetValue())

#endcap_MSVtx1 = goodTrigger.Filter('MSVtx_endcap_hits_eta.size() == 1 && nMSVtx == 1')
#entries = endcap_MSVtx1.Count()
#print("%s entries passed msvtx endcap hits cut == 1" %entries.GetValue())
#endcap_MSVtx1JetIso = endcap_MSVtx1.Filter('var1_var2_match1(0.6,MSVtx_endcap_hits_eta,MSVtx_endcap_hits_phi,\
#loose_cluster_jets_eta,loose_cluster_jets_phi) == 0')
#entries = endcap_MSVtx1JetIso.Count()
#print("%s entries passed jet isolated msvtx endcap hits cut == 1" %entries.GetValue())
#endcap_MSVtx1TrackIso = endcap_MSVtx1JetIso.Filter('var1_var2_match1(0.6,MSVtx_endcap_hits_eta,MSVtx_endcap_hits_phi,\
#track_g5_eta,track_g5_phi) == 0')
#entries = endcap_MSVtx1TrackIso.Count()
#print("%s entries passed track isolated msvtx endcap hits cut == 1" %entries.GetValue())
#endcap_MSVtx1TrackSumIso = endcap_MSVtx1TrackIso.Filter('var1_var2_ptsum(0.2,MSVtx_endcap_hits_eta,MSVtx_endcap_hits_phi,\
#track_l5_eta,track_l5_phi, track_l5_pt) < 10000.')
#entries = endcap_MSVtx1TrackSumIso.Count()
#print("%s entries passed track sum isolated msvtx endcap hits cut == 1" %entries.GetValue())

#barrel_Cluster1MSVtx1 = goodTrigger.Filter('MSVtx_barrel_hits_eta.size() == 1 && nMSVtx == 1')
#barrel_Cluster1MSVtx1_match = barrel_Cluster1MSVtx1.Filter('var1_var2_match1(0.4,MSVtx_barrel_hits_eta, MSVtx_barrel_hits_ph#i, muRoIClus_eta, muRoIClus_phi) == 1')
#entries = barrel_Cluster1MSVtx1_match.Count()
#print("%s entries passed barrel matched RoICluster and MSVtx == 1" %entries.GetValue())

#endcap_Cluster1MSVtx1 = goodTrigger.Filter('MSVtx_endcap_hits_eta.size() == 1 && nMSVtx == 1')
#endcap_Cluster1MSVtx1_match = endcap_Cluster1MSVtx1.Filter('var1_var2_match1(0.4,MSVtx_endcap_hits_eta, MSVtx_endcap_hits_ph#i,\
#muRoIClus_eta, muRoIClus_phi) == 1')
#entries = endcap_Cluster1MSVtx1_match.Count()
#print("%s entries passed endcap matched RoICluster and MSVtx == 1" %entries.GetValue())

# for each llp, ask if there is a muRoIClus with dr of 0.4
# if no, set the index = -1.  if yes, set the index to the llp

#df_llp_muroi = df.Define('idxmsvtx_muroi','llp_muonroi_dr(llp_eta,llp_phi,L1MuRoI_eta,L1MuRoI_phi)')

#print (type(idxmsvtx_muroi))

# vtx tracks
#df_tracks_1muonroi = df_defined.Filter('nmuroi == 1','one muon roi')\
#                      .Filter('nmsvtx > 0','at least one msvtx')\
#                      .Define('idxroi','Combinations(L1MuROI_eta,MSVtx_eta,L1MuRoI_eta,L1MuRoI_phi)')

ROOT.gStyle.SetOptStat(111111)
print ('step 6')

if (datatype == 3 or datatype == 20 or datatype == 10):
    h_NPV.Write()
    h_njets.Write()
    h_njets_noweight.Write()
    h_njets_noweight_barrel.Write()
    h_njets_noweight_endcap.Write()
    h_njets_njets_barrel.Write()
    h_njets_njets_endcap.Write()
    h_jet_pt_GeV.Write()
    h_jet_lograt.Write()
    h_jet_HECF.Write()
    h_log10_jetsliceweight.Write()
    h_jetsliceweight.Write()
    h_met_GeV.Write()
    h_nMSVtx.Write()
    h_nMSVtx_hits.Write()
    h_nMSVtx_barrel_hits.Write()
    h_nMSVtx_endcap_hits.Write()
    h_MSVtx_barrel_hits_vec_jet_drmin.Write()
    h_MSVtx_barrel_hits_vec_jet_pt.Write()
    h_MSVtx_barrel_hits_vec_jet_idx.Write()
    h_MSVtx_barrel_hits_vec_jet_nmdt.Write()
    h_MSVtx_barrel_hits_vec_jet_nrpc.Write()
    h_MSVtx_barrel_hits_nMSeg_eta.Write()
    h_MSVtx_barrel_hits_nMSTracklet_eta.Write()
    h_MSVtx_barrel_hits_g5track_drmin.Write()
    h_MSVtx_barrel_hits_g5track_ptsum_opposite.Write()
    h_MSVtx_barrel_hits_dr_mean_MSeg.Write()
    h_MSVtx_barrel_hits_dr_rms_MSeg.Write()
    h_MSVtx_barrel_hits_dr_mean_MSTracklet.Write()
    h_MSVtx_barrel_hits_dr_rms_MSTracklet.Write()
    h_MSVtx_barrel_hits_met_dphi.Write()

    h_MSVtx_endcap_hits_vec_jet_drmin.Write()
    h_MSVtx_endcap_hits_vec_jet_pt.Write()
    h_MSVtx_endcap_hits_vec_jet_idx.Write()
    h_MSVtx_endcap_hits_vec_jet_nmdt.Write()
    h_MSVtx_endcap_hits_vec_jet_ntgc.Write()
    h_MSVtx_endcap_hits_nMSeg_eta.Write()
    h_MSVtx_endcap_hits_nMSTracklet_eta.Write()
    h_MSVtx_endcap_hits_g5track_drmin.Write()
    h_MSVtx_endcap_hits_g5track_ptsum_opposite.Write()
    h_MSVtx_endcap_hits_dr_mean_MSeg.Write()
    h_MSVtx_endcap_hits_dr_rms_MSeg.Write()
    h_MSVtx_endcap_hits_dr_mean_MSTracklet.Write()
    h_MSVtx_endcap_hits_dr_rms_MSTracklet.Write()
    h_MSVtx_endcap_hits_met_dphi.Write()


    print ('step 7')
    iskip7 = 1
    if (iskip7 == 0):
        print ('njet entries = ',h_njets.GetEntries())
        print ('njet noweight entries = ',h_njets_noweight.GetEntries())
        print ('njet noweight barrel entries = ',h_njets_noweight_barrel.GetEntries())
        print ('njet noweight endcap entries = ',h_njets_noweight_endcap.GetEntries())
        nbins = h_njets.GetNbinsX()
        print (nbins)
        sumweights = h_njets.Integral(0,nbins+1)
        print (sumweights)
        print ('all')
        bin1 = h_nMSVtx.GetBinContent(1)
        bin2 = h_nMSVtx.GetBinContent(2)
        bin3 = h_nMSVtx.GetBinContent(3)
        print (bin1)
        print (bin2)
        print (bin3)
        print (bin1+bin2+bin3)

        print ('all hits')
        bin1 = h_nMSVtx_hits.GetBinContent(1)
        bin2 = h_nMSVtx_hits.GetBinContent(2)
        bin3 = h_nMSVtx_hits.GetBinContent(3)
        print (bin1)
        print (bin2)
        print (bin3)
        print (bin1+bin2+bin3)

        print ('barrel hits')
        bin1 = h_nMSVtx_barrel_hits.GetBinContent(1)
        bin2 = h_nMSVtx_barrel_hits.GetBinContent(2)
        bin3 = h_nMSVtx_barrel_hits.GetBinContent(3)
        print (bin1)
        print (bin2)
        print (bin3)
        print (bin1+bin2+bin3)

        print ('endcap hits')
        bin1 = h_nMSVtx_endcap_hits.GetBinContent(1)
        bin2 = h_nMSVtx_endcap_hits.GetBinContent(2)
        bin3 = h_nMSVtx_endcap_hits.GetBinContent(3)
        print (bin1)
        print (bin2)
        print (bin3)
        print (bin1+bin2+bin3)
    
else:
    h_nMSeg.Write()
    h_MSeg_etaPos.Write()
    h_nMSTracklet.Write()
    h_MSTracklet_eta.Write()
    h_nMSVtx_hits.Write()
    h_MSVtx_hits_eta.Write()
    h_MSVtx_barrel_hits_R.Write()
    h_MSVtx_barrel_hits_z.Write()
    h_MSVtx_barrel_hits_3dradius.Write()
    h_MSVtx_barrel_hits_jet_drmin.Write()
    h_MSVtx_barrel_hits_jet_pt.Write()
    h_MSVtx_barrel_hits_jet_logratio.Write()
    h_MSVtx_barrel_hits_jet_hecf.Write()
    h_MSVtx_barrel_hits_jet_nmdt.Write()
    h_nMSVtx_barrel_hits_MSeg_eta.Write()
    h_MSVtx_barrel_hits_track_drmin.Write()
    h_MSVtx_endcap_hits_R.Write()
    h_MSVtx_endcap_hits_z.Write()
    h_MSVtx_endcap_hits_3dradius.Write()
    h_MSVtx_endcap_hits_jet_drmin.Write()
    h_MSVtx_endcap_hits_jet_pt.Write()
    h_MSVtx_endcap_hits_jet_logratio.Write()
    h_MSVtx_endcap_hits_jet_hecf.Write()
    h_MSVtx_endcap_hits_jet_nmdt.Write()
    h_nMSVtx_endcap_hits_MSeg_eta.Write()
    h_MSVtx_endcap_hits_track_drmin.Write()


if (datatype == 2):
  h_llp_dR.Write()
  h_llp_Lxy.Write()
  h_llp_Lz.Write()
  h_llp_Lr.Write()
  h_MSVtx_hits_llp_drmin.Write()
  h_MSVtx_hits_llp_Lxy.Write()
  h_MSVtx_hits_llp_Lz.Write()
  h_MSVtx_hits_llp_nmdt.Write()
  h_LLP_MSVtx_dxy.Write()
  h_LLP_MSVtx_dz.Write()

skip = 1
if (skip != 1):
    h_logratio_njets.Write()
    h_logratio_jet_pt.Write()
    h_leading_jet_pt.Write()
    h_leading_jet_pt_weighted.Write()
    h_leading_logratio_jet_pt.Write()
    h_MSTracklet_pT_jet_match.Write()
    h_MSTracklet_pT_logratio_jet_match.Write()
    h_nMSTracklet_pT_jet_match.Write()
    h_nMSTracklet_pT_logratio_jet_match.Write()
    h_nMSeg_etaPos_jet_match.Write()
    h_nMSeg_etaPos_logratio_jet_match.Write()
    h2_nMSTracklet_pT_leading_jet_match.Write()
    h2_nMSTracklet_pT_leading_logratio_jet_match.Write()
    h2_nMSeg_etaPos_leading_jet_match.Write()
    h2_nMSeg_etaPos_leading_logratio_jet_match.Write()
    h_nMSTracklet.Write()
    h_nMSeg.Write()
    h_nmuRoIClus.Write()
    h_nMSVtx.Write()
    h_nMSVtx_barrel.Write()
    h_nMSVtx_barrel_hits.Write()
    h_nMSVtx_iso_hits.Write()
    h_njets_loose_cluster.Write()
    h_MSVtx_hits_jet_drmin.Write()
    h_MSVtx_hits_jet_pt.Write()
    h_MSVtx_hits_jet_logratio.Write()
    h_MSVtx_hits_jet_nmdt.Write()

# uncomment these 2 for atlas style
#ROOT.SetAtlasStyle()
#c1 = ROOT.TCanvas("c1","c1",0,0,800,600)

#ROOT.gROOT.LoadMacro("AtlasStyle.C")
#ROOT.gROOT.LoadMacro("AtlasLabels.C")
#ROOT.gROOT.LoadMacro("AtlasUtils.C")
# these 3 dont seem to work when you do LoadMacro
#ROOT.gStyle.SetOptStat(0)
#ROOT.gStyle.SetPadTickX(1)
#ROOT.gStyle.SetPadTickY(1)


#h_NPV.Draw()
#h_NPV.GetYaxis().SetTitle('Number')
#h_NPV.GetXaxis().SetTitle('Number of interactions per crossing')
#ROOT.ATLASLabel(0.6,0.8,'Preliminary')
#c1.Print("hist.pdf(","pdf")

#h_NPV.Draw()
#h_NPV.GetYaxis().SetTitle('Number')
#h_NPV.GetXaxis().SetTitle('Number of interactions per crossing')
#ROOT.ATLASLabel(0.6,0.8,'Preliminary')
#c1.Print("hist.pdf","pdf") 

#h_NPV.Draw()
#h_NPV.GetYaxis().SetTitle('Number')
#h_NPV.GetXaxis().SetTitle('Number of interactions per crossing')
#ROOT.ATLASLabel(0.6,0.8,'Preliminary')
#c1.Print("hist.pdf)","pdf")

#h_llp_Lx0.Write()
#h_llp_dr.Write()
#h_njets.Write()
#h_nl1muroi.Write()
#h_nmuroiclus.Write()
#h_nmuons.Write()
#h_ntruth.Write()
#h_nmsvtx.Write()
#h_ntracks.Write()
#h_tracks_pt400.Write()
#h_ntracks_pt400.Write()
#h_llp_eta_barrel.Write()
#h_llp_lxy_barrel_fid.Write()
#h_llp_lxy_endcap_fid.Write()
#h_nllp_barrel_fid.Write()
#h_nllp_endcap_fid.Write()
#h_llp_lxy_barrel_l1muroi.Write()
#h_llp_lxy_endcap_l1muroi.Write()
#h_llp1_lxy_barrel_muroiclus.Write()
#h_llp1_lxy_endcap_muroiclus.Write()
#h_llp_lxy_barrel_msvtx.Write()
#h_llp_lxy_endcap_msvtx.Write()
#h_llp_lxy.Write()
#h_llp_lz.Write()
#h_llp_beta.Write()
#h_llp_gamma.Write()
#h_llp_pt.Write()

#h_msvtx_closejetdr.Write()
#h_msvtx_hightrackdr.Write()
#h_msvtx_lowtrackdr.Write()
#h_msvtx_nmdt.Write()

#h_msvtx_sumtrackptcone.Write()

print ('step 8')
myfile.Close()

# test variables for barrel
# drjet, ptjet, nmdt+nrpc, drtrack

#h_MSVtx_barrel_hits_vec_jet_drmin

df_barrel_filtered = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_barrel_hits_drmin_cuts1).Filter(msvtx_barrel_hits_cut1)
npy_barrel = df_barrel_filtered.AsNumpy(columns=["bdt_target",\
                                   "MSVtx_barrel_hits_vec_jet_drmin","MSVtx_barrel_hits_vec_jet_pt",\
                                   "MSVtx_barrel_hits_vec_jet_nmdt","MSVtx_barrel_hits_vec_jet_nrpc",\
                                   "MSVtx_barrel_hits_g5track_drmin","MSVtx_barrel_hits_g5track_ptsum_opposite",\
                                   "MSVtx_barrel_hits_dr_mean_MSeg","MSVtx_barrel_hits_dr_rms_MSeg",\
                                   "MSVtx_barrel_hits_dr_mean_MSTracklet","MSVtx_barrel_hits_dr_rms_MSTracklet",\
                                   "MSVtx_barrel_hits_met_dphi"])
print (type(npy_barrel))
print ('shape')
print (np.shape(npy_barrel))

panda_df_barrel = pandas.DataFrame(npy_barrel)
panda_df_barrel["bdt_sample_type"]=datatype
panda_df_barrel["h_mass"]=int(h_mass)
panda_df_barrel["s_mass"]=int(s_mass)
print (panda_df_barrel.shape)
if (datatype == 20):
    panda_df_barrel.to_csv('panda_barrel_'+signalname+date+'.csv',index=False)
elif (datatype == 3):
    panda_df_barrel.to_csv('panda_barrel_jet_all-'+date+'.csv',index=False)
elif (datatype == 10):
    panda_df_barrel.to_csv('panda_barrel_data_'+data_year+'-'+date+'.csv',index=False)

df_endcap_filtered = df_defined.Filter(clean_cut1).Filter(met_cut1).Filter(msvtx_endcap_hits_drmin_cuts1).Filter(msvtx_endcap_hits_cut1)
npy_endcap = df_endcap_filtered.AsNumpy(columns=["bdt_target",\
                                   "MSVtx_endcap_hits_vec_jet_drmin","MSVtx_endcap_hits_vec_jet_pt",\
                                   "MSVtx_endcap_hits_vec_jet_nmdt","MSVtx_endcap_hits_vec_jet_ntgc",\
                                   "MSVtx_endcap_hits_g5track_drmin","MSVtx_endcap_hits_g5track_ptsum_opposite",\
                                   "MSVtx_endcap_hits_dr_mean_MSeg","MSVtx_endcap_hits_dr_rms_MSeg",\
                                   "MSVtx_endcap_hits_dr_mean_MSTracklet","MSVtx_endcap_hits_dr_rms_MSTracklet",\
                                   "MSVtx_endcap_hits_met_dphi"])
# removed to speed up
# "MSVtx_endcap_hits_nMSeg_eta","MSVtx_endcap_hits_nMSTracklet_eta",\

print (type(npy_endcap))
print ('shape')
print (np.shape(npy_endcap))

panda_df_endcap = pandas.DataFrame(npy_endcap)
panda_df_endcap["bdt_sample_type"]=datatype
panda_df_endcap["h_mass"]=int(h_mass)
panda_df_endcap["s_mass"]=int(s_mass)
print (panda_df_endcap.shape)
if (datatype == 20):
    panda_df_endcap.to_csv('panda_endcap_'+signalname+date+'.csv',index=False)
elif (datatype == 3):
    panda_df_endcap.to_csv('panda_endcap_jet_all-'+date+'.csv',index=False)
elif (datatype == 10):
    panda_df_endcap.to_csv('panda_endcap_data_'+data_year+'-'+date+'.csv',index=False)

# python3 only
#toc = time.perf_counter()
toc = time.time()
# minutes
print ('elasped minutes ',(toc-tic)/60.)
print ('done!')
